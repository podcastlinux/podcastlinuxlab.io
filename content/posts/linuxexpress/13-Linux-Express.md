---
title: "#13 Linux Express"
author: Juan Febles
date: 2017-05-17
categories: [linuxexpress]
img: 2017/13LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/13linuxexpress
  olength : 3292956
  mlength : 3033786
  iduration : "00:06:19"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE13
---
Aquí tienes otra entrega de Linux Express, el podcast creado de los audios de Telegram, para que estés informado de lo que se cuece en Podcast Linux.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/13linuxexpress.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>

Hoy quiero compartir contigo estos temas:

+ [Episodio #24 Linux Connexion con David OchoBits](http://avpodcast.net/podcastlinux/davidochobits)
+ Próximo episodio GNU/Linux y NAS
+ [Slimbook One](https://slimbook.es/one-minipc-potente)
+ [Post KDE Blog](https://kdeblog.com/podcast-linux-y-la-educacion.html)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.github.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
