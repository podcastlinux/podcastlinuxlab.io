---
title: "#116 Linux Express"
date: 2021-03-31
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/116linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4605426
  mlength : 4533227
  iduration : "00:09:26"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE116
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/116linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/116linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #126 [PYMES y Software Libre](https://podcastlinux.com/PL126)
+ Siguiente episodio: Linux Connexion con Francisco Javier Miró
+ Vídeos de la [1ª Cumbre Software Libre y Educación](https://devtube.dev-wiki.de/video-channels/eventos/videos)
+ Directo sobre Linux en [Cacharreo Geek](https://youtu.be/P7kNKc9D1Js)
+ Nuevo Thinkpad T440P
+ Escucha el Podcast [Desahogo Geek](https:// anchor.fm/s/1218850/podcast/rss)
+ Ya está aquí [Audacity 3.0](https://www.audacityteam.org/audacity-3-0-0-released/)
+ Servicio [CryptPad](https://cryptpad.fr/)
+ Eventos a tener en cuenta: [Maratón Pod](http://maratonpod.com), [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)
+ Avanzando en la [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
