---
title: "#178 Linux Express"
date: 2023-08-16
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/178linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5733891
  mlength : 5488681
  iduration : "00:11:25"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE178
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/178linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/178linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #188 [Linux Connexion con Carles GeeksTV](https://podcastlinux.com/PL188)
+ Probando [Brave](https://mastodon.social/@podcastlinux/110812476494777863) como navegador secundario
+ Un [HD externo](https://mastodon.social/@podcastlinux/110823801080889173) nunca viene mal
+ Primer error al actualizar [Arch Linux](https://mastodon.social/@podcastlinux/110836069288934934)
+ [Caissabase](), base de datos libres para aprender ajedrez
+ Distros inmutables, [¿sí o no?](https://mastodon.social/@podcastlinux/110852112517495493)
+ De [vacaciones con GNU/Linux](https://mastodon.social/@podcastlinux/110863437019100290)
+ [Lichess](https://mastodon.social/@podcastlinux/110875705558874949), mi elección libre para aprender y entrenar al ajedrez
+ Ya somos [1700 en Twitter](https://mastodon.social/@podcastlinux/110881367748330273)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
