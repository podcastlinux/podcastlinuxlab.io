---
title: "#58 Linux Express"
date: 2019-01-09
author: Juan Febles
categories: [linuxexpress]
img: 2019/58linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/58linuxexpress
  olength : 3603526
  mlength : 4425728
  iduration : "00:09:03"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE58
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/58linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/58linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#68 Especial Slimbook Eclipse](https://avpodcast.net/podcastlinux/slimbookeclipse/)
+ Próximo episodio: Linux Connexion con [Aleix Pol](https://twitter.com/aleixpol)
+ Instalando [KDE Neon](https://neon.kde.org/) desde cero en mi [Slimbook One](https://slimbook.es/one-minipc-potente)
+ Instalando [Retropie](https://retropie.org.uk/) en PC [GNU/Linux](https://twitter.com/podcastlinux/status/1080741326289862656)
+ Territorio F-Droid: [Neoid](https://f-droid.org/es/packages/com.androidemu.nes/)
+ Me he pillado la [Playastation Mini](https://www.playstation.com/es-es/explore/playstation-classic/)
+ Ya está aquí el [Xiaomi Mi8 Lite](https://www.mi.com/es/mi-8-lite)
+ [FLISol Tenerife](https://flisol.info/FLISOL2019/Espana/Tenerife) en marcha
+ Feliz 2019


Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1889081).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
