---
title: "#125 Linux Express"
date: 2021-08-04
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/125linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5490743
  mlength : 5352009
  iduration : "00:11:08"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE125
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/125linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/125linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #135 [Linux Connexion con Érica Aguado](https://podcastlinux.com/PL135)
+ Siguiente episodio: Linux Connexion con María Arias de Reyna
+ Actualizando todas las paqueterías desde la [terminal](https://twitter.com/podcastlinux/status/1420633283487010821)
+ Adios al [Slimbook One AMD](https://slimbook.es/one)
+ Charla [Alejandro López en Atareao](https://atareao.es/podcast/comunidad-linux-con-alejandro-lopez-slimbook-y-el-derecho-a-reparar/)
+ [Audacity 3.0.3](https://www.audacityteam.org/audacity-3-0-3-is-out-now/)
+ Gestor de Podcast KDE [Kasts](https://apps.kde.org/es/kasts/)
+ Podcast [KDE Express](https://kdeexpress.gitlab.io/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
