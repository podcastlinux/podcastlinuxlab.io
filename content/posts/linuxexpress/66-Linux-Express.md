---
title: "#66 Linux Express"
date: 2019-05-01
author: Juan Febles
categories: [linuxexpress]
img: 2019/66linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/66linuxexpress
  olength : 3006306
  mlength : 3647488
  iduration : "00:07:25"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE66
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/66linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/66linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#76 Especial FLISOL 2019](https://avpodcast.net/podcastlinux/flisol2019/)
+ Próximo episodio: Especial [FLISOL Tenerife 2019](https://flisol.info/FLISOL2019/Espana/Tenerife)
+ [Maratón Linuxero](https://maratonlinuxero.org/) 20 de abril
+ Presentaciones con Markdown
+ Proyecto NAS Libre
+ Componentes chinos para mejorar PC
+ Territorio f-Droid: [Markor](https://f-droid.org/en/packages/net.gsantner.markor/)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-3942956/).
El cartel FLISoL Tenerife es propiedad de [Manz](https://www.emezeta.com/manz/) y liberado con [licencia Creative Commons Atribución - Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/deed.es)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
