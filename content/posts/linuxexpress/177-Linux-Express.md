---
title: "#177 Linux Express"
date: 2023-08-02
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/177linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5536416
  mlength : 5334448
  iduration : "00:10:58"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE177
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/177linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/177linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #187 [Linux Connexion con Linuxero Errante](https://podcastlinux.com/PL187)
+ Proponiendo [Linux Mint](https://mastodon.social/@podcastlinux/110733204126626199) como distro para iniciarse
+ [Linux Mint LMDE](https://mastodon.social/deck/@podcastlinux/110796454387605193) para ordenadores 32 bits
+ Celebrando el [día mundial del Ajedrez](https://mastodon.social/@podcastlinux/110744528644353787)
+ [Curso](https://mastodon.social/deck/@podcastlinux/110762459172867545) de Base de datos de ajedrez privativo
+ [Perdónate Tux](https://mastodon.social/@podcastlinux/110756797154047637), he pecado...
+ Epidosio sobre Telemetría de [Compilando Podcast](https://compilando.audio/index.php/2023/07/24/telemetria-si-telemetria-no/)
+ PodcastLinux en la red libre de música [Funkwhale](https://mastodon.social/deck/@podcastlinux/110784164739443147)
+ Realizando nuevas sintonías con [LMMS](https://mastodon.social/@podcastlinux/110802095793182365)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
