---
title: "#105 Linux Express"
date: 2020-10-28
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/105linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5477459
  mlength : 5257759
  iduration : "00:10:56"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE105
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/105linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/105linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #115 [Hardware Slimbook Essential](https://podcastlinux.com/PL115)
+ Siguiente episodio: Personalizando GNU/Linux
+ "Podcasting Libre con Audacity" en [HacktoberDay 2020](https://archive.org/details/charlaaudacity20 )
+ [Ubuntu 20.10](https://ubuntu.com/)
+ Actualizaciones en [KDE Neon 5.20](https://blog.neon.kde.org/index.php/2020/10/13/kde-neon-5-20/)
+ Probando de nuevo [Tusky](https://tusky.app/)
+ Se ha solucionado el feed en [Google Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ)
+ Nueva [Raspberry 4 Compute Module](https://www.raspberrypi.org/blog/designing-the-raspberry-pi-compute-module-4)
+ Presenta tu charla para [Akademy-es 2020](https://www.kdeblog.com/ampliado-el-plazo-para-presentar-tu-charla-para-akademy-es-2020-en-linea.html)
+ Evento [24H24L](https://www.24h24l.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
