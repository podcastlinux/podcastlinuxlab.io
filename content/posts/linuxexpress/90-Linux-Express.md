---
title: "#90 Linux Express"
date: 2020-04-01
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/90linuxexpress
  olength : 4181029
  mlength : 5232640
  iduration : "00:10:38"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE90
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/90linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/90linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #100 [Especial Preguntas y Respuestas](https://avpodcast.net/podcastlinux/100)
+ Siguiente episodio: Especial VPS Educativo Libre
+ Episodio con [KDE España Podcast](https://youtu.be/c9AFefcDuMQ)
+ Probando Editores visuales en bloque de Arduino: [ArduinoBlocks](http://www.arduinoblocks.com/) y [Mblock](https://www.mblock.cc/en-us/)
+ Nuevos proyectos con [Arduino](https://twitter.com/podcastlinux/status/1243892108785786882)
+ Nueva versión de [OBS Studio](https://obsproject.com/es) y tutorial de [9Decibelios](https://youtu.be/AgGiDQGUI-E)
+ Asistencia virtual a [MaratónPod](https://www.maratonpod.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
