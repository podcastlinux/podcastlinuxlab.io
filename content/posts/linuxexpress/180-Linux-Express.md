---
title: "#180 Linux Express"
date: 2023-09-13
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/180linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5367935
  mlength : 5040623
  iduration : "00:10:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE180
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/180linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/180linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #190 [Hardware Miyoo Mini Plus](https://podcastlinux.com/PL190)
+ Episodio #191 Linux Connexion  con Ninoh-FOX
+ Ya tenemos bien calentito el [Kernel 6.5](https://mastodon.social/@podcastlinux/110971021100521243) con muchas mejoras
+ Miyoo Mini Plus con [funda](https://mastodon.social/@podcastlinux/110982346113463323)
+ Koriki, custom firmware nacional para las [Miyoo Mini](https://mastodon.social/@podcastlinux/110994614157716975)
+ [Brothers of Metal](https://mastodon.social/@podcastlinux/111000276190868337), una imagen repleta de retrojuegos
+ Las dos versiones de la [Miyoo Mini](https://mastodon.social/@podcastlinux/111010657238285762)
+ Revisar paquetes nuevos en [Arch Linux](https://mastodon.social/@podcastlinux/111021981703635538)
+ Muchos eventos de temática libre en Galicia: [AtlanticaConf 2023](https://atlanticaconf.com), [GeoCamp 2023]( https://www.nosolosig.com/geocamp-es-2023) y [Oshwdem 2023](https://oshwdem.org)
+ Traduce textos directamente desde [Firefox](https://mastodon.social/@podcastlinux/111039912499377680)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
