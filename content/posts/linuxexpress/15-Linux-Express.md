---
title: "#15 Linux Express ¿Mp3 o Ogg? y 2"
author: Juan Febles
date: 2017-05-21
categories: [linuxexpress]
img: 2017/15LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/15linuxexpress
  olength : 3970830
  mlength : 3637198
  iduration : "00:07:34"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE15
---
Mi gozo en un pozo. Fin del experimento. El Mp3 está vivo, muy vivo. Escucha mis conclusiones sobre el uso del ogg en el podcasting.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/15linuxexpress.mp3" type="audio/mpeg">
</audio>

Si no pudiste escuchar el anterior episodio por estar codificado en ogg, aquí lo tienes en mp3.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/14linuxexpress.mp3" type="audio/mpeg">
</audio>


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
