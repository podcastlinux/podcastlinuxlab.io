---
title: "#157 Linux Express"
date: 2022-10-26
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/157linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6131389
  mlength : 5771222
  iduration : "00:12:01"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE157
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/157linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/157linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #167 [Hardware Micro:bit](https://podcastlinux.com/PL167)
+ Siguiente episodio: Linux Connexion con Iraisy Figueroa
+ [Atareao con Linux](https://nitter.net/podcastlinux/status/1581362488573980672#m) gana en las Jpod
+ Ya está aquí [Plasma 5.16](https://nitter.net/podcastlinux/status/1580421179818790912#m)
+ Camiseta [KDE Akademy-es](https://nitter.net/podcastlinux/status/1582227471440564227#m)
+ Probando [Astrofox](https://nitter.net/podcastlinux/status/1584764188450328576#m) para audigramas elaborados
+ [Ardour 7](https://ardour.org/) con problemas en flatpak
+ ¿Conseguiremos terminar el [#PCRecicladoDeLaAudiencia](https://podcastlinux.com/pcreciclado)?
+ Disfrutando del [Ajedrez en GNU/Linux](https://nitter.net/podcastlinux/status/1582952247872425984#m)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
