---
title: "Crossover Linuxero KilallRadio"
author: Juan Febles
date: 2017-06-21
categories: [linuxexpress]
img: 2017/crossover.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/20CrossoverLinuxeroDirectoSobreGNOMEPlasmaYOtrasHierbas/20_crossover_linuxero_directo_sobre_gnome_plasma_otras_hierbas
  olength :
  mlength :
  iduration :
tags: [audio, telegram, Linux Express,]
comments: true
---

![]({{< ABSresource url="/images/2017/crossover.png" >}})

Hoy traigo un nuevo podcast, un crossover que realizamos Yoyo, Elav, Paco Estrada, Dj Mao Mix y Richie... y el querido perro de mi vecino... Risas y más risas y una charla interesante con muy buenos amigos de Tux.

<audio controls>
  <source src="https://archive.org/download/20CrossoverLinuxeroDirectoSobreGNOMEPlasmaYOtrasHierbas/20_crossover_linuxero_directo_sobre_gnome_plasma_otras_hierbas.ogg" type="audio/ogg">
  <source src="https://archive.org/download/20CrossoverLinuxeroDirectoSobreGNOMEPlasmaYOtrasHierbas/20_crossover_linuxero_directo_sobre_gnome_plasma_otras_hierbas.mp3" type="audio/mpeg">
</audio>


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
