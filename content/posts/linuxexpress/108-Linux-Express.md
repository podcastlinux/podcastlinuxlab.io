---
title: "#108 Linux Express"
date: 2020-12-09
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/108linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6541064
  mlength : 6207780
  iduration : "00:12:55"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE108
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/108linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/108linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #118 [Linux Connexion con 24H24L](https://podcastlinux.com/PL118)
+ Siguiente episodio: Linux Connexion con Alejandro López
+ AMD detrás de prototipos [ARM tipo M1](https://www.muycomputer.com/2020/12/03/amd-alternativa-al-soc-apple-m1/)
+ [Atareao y la Raspberry Pi 400](https://www.atareao.es/podcast/la-pi-400-es-brutal/)
+ [Crossover Mosqueteroweb y Ugeek](https://go.ivoox.com/rf/61352811)
+ [Doking para Discos Duros](https://a.aliexpress.com/_BSOJVz)
+ Canal Telegram [#ViernesDeEscritorio](https://t.me/fridaydesktop) gracias a [Lina Castro](https://twitter.com/lirrums)
+ Flisol 2021 España online. ¿Te apuntas?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
