---
title: "#176 Linux Express"
date: 2023-07-19
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/176linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6678461
  mlength : 6330863
  iduration : "00:13:02"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE176
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/176linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/176linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #185 [Linux Connexion con Antonio Manfredi](https://podcastlinux.com/PL186)
+ Siguiente episodio: Linux Connexions de verano
+ Mastodon, mi [red social principal](https://mastodon.social/@podcastlinux/110653931717275757)
+ [Somos 1.600](https://mastodon.social/@podcastlinux/110717160693683095) en Mastodon
+ Nuevo episodio de [Compilando Podcast](https://mastodon.social/@podcastlinux/110665256181739678) del gran Paco Estrada
+ ¿Cuál es tu [comando de terminal](https://mastodon.social/@podcastlinux/110677524699927889) favorito?
+ Los [videojuegos](https://mastodon.social/@podcastlinux/110683187312379031) son para el verano
+ ¿Cómo creas [contraseñas](https://mastodon.social/@podcastlinux/110693568164343113) seguras y fácil de recordar?
+ [Taller de Micro:Bit](https://mastodon.social/@podcastlinux/110704892393974321) en el colegio
+ [Lucas Chess](https://mastodon.social/@podcastlinux/110722823336101318), Software Libre para entrenar al ajedrez.

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
