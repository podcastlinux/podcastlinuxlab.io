---
title: "#119 Linux Express"
date: 2021-05-12
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/119linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6561421
  mlength : 6383114
  iduration : "00:13:17"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE119
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/119linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/119linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #129 [Hardware ThinkPad](https://podcastlinux.com/PL129)
+ Siguiente episodio: Linux Connexion con BoteDeGel
+ Vídeos de [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)
+ Yoyo nos muestra cómo funciona [Pipewire](https://salmorejogeek.com/tag/pipewire/)
+ Compra y telemetría en [Audacity](https://www.muylinux.com/2021/05/07/audacity-telemetria/)
+ Directo en junio a través de [Peertube](https://devtube.dev-wiki.de/accounts/podcastlinux)
+ [Streaming con Software libre](https://devtube.dev-wiki.de/videos/watch/playlist/9736dd3e-e235-4afa-87d6-d2d70ea3b523?playlistPosition=7&resume=true) por David Marzal
+ Podcast [NoVaDeLinux](https://novadelinux.es/)
+ Eventos a tener en cuenta: [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
