---
title: "#140 Linux Express"
date: 2022-03-02
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/140linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5055001
  mlength : 4975846
  iduration : "00:10:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE140
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/140linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/140linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #150 [Linux Connexion con Slimbook](https://podcastlinux.com/PL150)
+ Siguiente episodio: [Linux Connexion con David Vargas](github.com/dvarrui)
+ Proyecto Comunidad VoroMV en [Gitlab](https://gitlab.com/Comunidad-VoroMV)
+ Charla sobre Distros GNU/Linux en el podcast [Buhardilla Geek](https://www.ivoox.com/podcast-buhardilla-geek_sq_f1465450_1.html)
+ KDE Neon o Ubuntu Studio
+ Nuevo SSD para mi PC de Sobremesa
+ Activar detector de huellas en [KDE Neon](https://twitter.com/kdecommunity/status/1496807516843520004)
+ Migración del canal Peertube a [FediverseTV](https://fediverse.tv/a/podcastlinux)
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
