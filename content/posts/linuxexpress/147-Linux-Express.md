---
title: "#147 Linux Express"
date: 2022-06-08
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/147linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5843193
  mlength : 5729844
  iduration : "00:11:56"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE147
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/147linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/147linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #157 [Especial #ViernesDeEscritorio](https://podcastlinux.com/PL157)
+ Siguiente episodio: Linux Connexion con [esLibre](https://eslib.re/2022/)
+ Automatizar publicaciones en [redes sociales](https://twitter.com/podcastlinux/status/1528358573817970689)
+ [Wordle libre](https://wordle.global/es)
+ Probando Ardour con [Pipeware](https://twitter.com/podcastlinux/status/1529782506517561344)
+ Extensiones para [Firefox](https://twitter.com/yoyo308/status/1530843766453678080)
+ Ubuntu 22.10 vendrá con [Pipeware por defecto](https://www.omgubuntu.co.uk/2022/05/ubuntu-22-10-makes-pipewire-default)
+ Fin de semana de puente con mi [Thinkpad X260](https://twitter.com/podcastlinux/status/1530858424753393664)
+ Jugando a [Sir Ababol](https://twitter.com/podcastlinux/status/1531498899797643266)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
