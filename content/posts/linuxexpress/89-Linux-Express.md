---
title: "#89 Linux Express"
date: 2020-03-18
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/89linuxexpress
  olength : 4060576
  mlength : 5179392
  iduration : "00:10:31"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE89
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/89linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/89linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #99 [Linux Connexion con EsLibre](https://avpodcast.net/podcastlinux/eslibre)
+ Siguiente episodio: Especial episodio 100
+ Visita a [Stallman a Valencia aplazada](https://twitter.com/GnuLinuxVlc/status/1238494651897786372)
+ Muchos eventos aplazados: [Flisol](https://flisol.info/), [Open South Code](https://www.opensouthcode.org/conferences/opensouthcode2020), [Arduino Day](https://day.arduino.cc/)
+ Propuesta de Pedro Mosquetero Web: [Plataforma libre de Educación a distancia](https://twitter.com/mosqueteroweb/status/1238432123339628545). Telegram: <https://t.me/joinchat/AFleDR2PRhok3jo1e3Rfgg>
+ Episodio [Teletrabajo y Software Libre](https://www.ivoox.com/48885611) de [Ubuntu y otras Hierbas](https://www.ivoox.com/podcast-ubuntu-otras-hierbas_sq_f1412582_1.html)
+ Personaliza tus mvk con [MKVToolNix](https://mkvtoolnix.download/)
+ Cursos de [Software Libre](https://twitter.com/podcastlinux/status/1239111214170595329) para el confinamiento
+ Preguntas y respuestas para el episodio 100

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
