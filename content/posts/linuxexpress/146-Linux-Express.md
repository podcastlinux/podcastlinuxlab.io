---
title: "#146 Linux Express"
date: 2022-05-25
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/146linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5721624
  mlength : 5352218
  iduration : "00:11:08"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE146
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/146linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/146linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #156 [Linux Connexion con Academia Automatización del Hogar](https://podcastlinux.com/PL156)
+ Siguiente episodio: Especial [#ViernesDeEscritorio](https://twitter.com/search?q=%23ViernesDeEscritorio&src=typeahead_click)
+ Cómo se inició [#ViernesDeEscritorio](https://twitter.com/podcastlinux/status/1525406644351422464)
+ 8.000 seguidores en [Twitter](https://twitter.com/podcastlinux)
+ Probando un [dispositivo mutisensor](https://twitter.com/podcastlinux/status/1523888660869001216) con Tasmota
+ Reciclando [cables](https://twitter.com/podcastlinux/status/1524612402720956418) para protoboard
+ [Eclipse lunar](https://twitter.com/podcastlinux/status/1527151845671686144) retransmitido 100% Software Libre
+ Montando un [altavoz Bluetooth](https://twitter.com/podcastlinux/status/1527910899188740097)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
