---
title: "#153 Linux Express"
date: 2022-08-31
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/153linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5199268
  mlength : 5145955
  iduration : "00:10:43"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE153
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/153linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/153linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #163 [Linux Connexion con FediverseTV](https://podcastlinux.com/PL163)
+ Siguiente episodio: 15 años con GNU/Linux
+ Probando el Alfa 3.2 de [Audacity](https://nitter.net/podcastlinux/status/1560144089647136770#m)
+ Audios de [Libre Home Gym](https://gitlab.com/podcastlinux/librehomegym)
+ Probando Autotune con [Gsnap en Audacity](https://nitter.net/podcastlinux/status/1562687343110529024#m)
+ [Rfxgen](https://nitter.net/podcastlinux/status/1560886610975137793#m) para crear tus efectos de sonidos retro.
+ Nueva versión de [Kdenlive 5.8](https://kdenlive.org/en/2022/08/kdenlive-22-08-released)
+ [Presenta tu charla en [Akademy-es](https://www.kdeblog.com/presenta-tu-charla-a-akademy-es-2022-de-barcelona-akademyes.html)
+ Vota GNU/Linux en las [Jpod](https://nitter.net/podcastlinux/status/1561959154801164288#m) 
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy Internacional](https://akademy.kde.org/2022) en Barcelona en octubre 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
