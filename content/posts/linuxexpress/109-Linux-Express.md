---
title: "#109 Linux Express"
date: 2020-12-23
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/109linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6640800
  mlength : 6398370
  iduration : "00:13:19"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE109
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/109linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/109linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #119 [Linux Connexion con Slimbook](https://podcastlinux.com/PL119)
+ Siguiente episodio: Hardware [Vant Block Lite](https://www.vantpc.es/block-lite)
+ [MegaPak Vant](https://youtu.be/gh7D6vvuRc0)
+ Crossover [Atareao](https://www.atareao.es/podcast/estrujando-docker-con-ugeek/) con [Ugeek](https://ugeek.github.io/post/2020-12-14-vim-vs-emacs-con-atareao.html)
+ Streaming Libre con [Owncast](https://owncast.online/) Marcel Costa desde Mastodon.
+ Domótica libre con [Jeedom](https://www.jeedom.com/en/)
+ Crea fondos de escritorios desde la [Terminal](https://twitter.com/linuxmanR4/status/1340325494143725568)
+ Feed [24H24L](https://24h24l.org/feed.xml)
+ Flisol 2021 España online. ¿Te apuntas?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
