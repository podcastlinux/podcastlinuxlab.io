---
title: "#129 Linux Express"
date: 2021-09-29
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/129linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7210086
  mlength : 6903891
  iduration : "00:14:22"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE129
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/129linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/129linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #139 [Domótica Libre con Home Assistant](https://podcastlinux.com/PL139)
+ Siguiente episodio: Linux Connexion con [Luis del Valle](https://programarfacil.com/)
+ Episodio Encuentro Podcast Linux con [Jam](https://jamshelf.com/)
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Error [initramfs](https://medium.com/@aterro51/linux-how-to-fix-linux-mint-initramfs-prompt-at-boot-a6bced4fe49f) al arrancar un pc
+ [Café para Dos](https://t.me/entrevistaendiferido/2171) con Carlos del Castillo llevado por José Jiménez
+ Fallece [Clive Sinclair](https://hipertextual.com/2021/09/muere-clive-sinclair)
+ Presenta tu charla en [Akademy-es](https://www.kdeblog.com/presenta-tu-charla-a-akademy-es-2021.html) 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
