---
title: "#104 Linux Express"
date: 2020-10-14
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/104linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5372823
  mlength : 5150135
  iduration : "00:10:43"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE104
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/104linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/104linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #114 [Linux Connexion Pásate a GNU/Linux](https://podcastlinux.com/PL114)
+ Siguiente episodio: [Hardware Slimbook Essential](https://slimbook.es/essential)
+ Vídeos de Slimbook Essential en [Youtube](https://www.youtube.com/podcastlinux)
+ ¿[Shotcut](https://www.shotcut.org/) o [KDEnLive](https://kdenlive.org/es/)?
+ Primeras pruebas de emisiones en directo con [OBS Studio](https://twitter.com/podcastlinux/status/1312784701254447107)
+ Error de actualización en [KDE Neon](https://twitter.com/podcastlinux/status/1314620090864807938)
+ Nueva versión de [AntennaPod](https://antennapod.org/)
+ "Podcasting Libre con Audacity" en [HacktoberDay 2020](https://hacktoberday.librelabucm.org/)
+ Evento [24H24L](https://www.24h24l.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
