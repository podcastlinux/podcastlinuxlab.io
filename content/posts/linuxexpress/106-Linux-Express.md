---
title: "#106 Linux Express"
date: 2020-11-11
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/106linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4821186
  mlength : 4714203
  iduration : "00:09:49"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE106
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/106linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/106linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #116 [Personalizando GNU/Linux](https://podcastlinux.com/PL116)
+ Siguiente episodio: Linux Connexion con Yoyo y Elav
+ Compras en el [11.11](https://es.aliexpress.com/)
+ Comentarios dentro del [canal de Telegram](https://t.me/podcastlinux)
+ Nueva [Raspberry Pi 400](https://www.raspberrypi.org/products/raspberry-pi-400)
+ Haz un vídeo de tus audios con [ffmpeg](https://twitter.com/podcastlinux/status/1325032158042468352?s=20)
+ Estaré en la [Akademy-es 2020](https://www.kde-espana.org/akademy-es-2020) hablando de Podcasting Libre
+ Evento [24H24L](https://www.24h24l.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
