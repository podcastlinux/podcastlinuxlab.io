---
title: "#156 Linux Express"
date: 2022-10-12
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/156linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4305761
  mlength : 4293736
  iduration : "00:08:56"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE156
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/156linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/156linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #166 [Linux Connexion con Eduardo Moreno](https://podcastlinux.com/PL166)
+ Siguiente episodio: Hardware Micro:bit
+ Tenemos más componentes del [PC Reciclado de la Audiencia](https://podcastlinux.com/pcreciclado)
+ Actualización [Audacity 3.2.1](https://nitter.net/podcastlinux/status/1579019280439738369#m)
+ Moderando los [comentarios](https://nitter.net/podcastlinux/status/1577154041422876672#m) en podcastlinux.com
+ Interesado en la [Raspberry PI Pico](https://nitter.net/podcastlinux/status/1579690756603727872#m)
+ Juegazo [Night Knight](https://nitter.net/podcastlinux/status/1577878818097725440#m) para MSX
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy Internacional](https://akademy.kde.org/2022)
+ Premios [Jpod](https://nitter.net/podcastlinux/status/1567007183178321920#m) este fin de semana

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
