---
title: "#100 Linux Express"
date: 2020-08-19
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/100linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5681252
  mlength : 5429857
  iduration : "00:11:03"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE100
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/100linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/100linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #110 [Linux Connexion con Carlos Arturo Guerra Parra](https://podcastlinux.com/PL110)
+ Siguiente episodio: Linux Connexion con [José Jiménez](http://rooteando.com/)
+ [KDE Neon 20.04](https://neon.kde.org/)
+ Dificultades procesadores [AMD 4000](https://twitter.com/SlimbookEs/status/1291678613683802112)
+ Cambio de mi set de escritorio.
+ [Akademy](https://akademy.kde.org/2020), [FLISOL A Coruña](https://flisol2020acoruna.gitlab.io/) y [esLibre](https://eslib.re/2020/) en septiembre
+ En vacaciones con mi [Thinkpad X220](https://twitter.com/podcastlinux/status/1294190909446336513)
+ Viciado al [Manic Miner](https://twitter.com/podcastlinux/status/1293894713133731840)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
