---
title: "#97 Linux Express"
date: 2020-07-08
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/97linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4978947
  mlength : 5245593
  iduration : "00:10:33"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE97
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/97linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/97linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #107 [Especial 4º Aniversario](https://podcastlinux.com/PL107)
+ Siguiente episodio: Linux Connexion con [Converso](https://www.vidasenred.com/p/acerca-de-mi.html)
+ Nueva web [Hugo](https://gohugo.io/) ya operativa: <https://podcastlinux.com>
+ Utilizando [Hackmd.io](https://hackmd.io/@podcastlinux) para notas del programa
+ Crear scrpits para automatizar procesos.
+ Probando [Linux Mint Cinnamon](https://linuxmint.com/)
+ Nueva versión de [Shotcut](https://shotcut.org/blog/new-release-200628/)
+ Tentado de quedarme con el [Vant Edge](https://www.vantpc.es/edge)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
