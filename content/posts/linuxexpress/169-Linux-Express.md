---
title: "#169 Linux Express"
date: 2023-04-12
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/169linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5615108
  mlength : 5475092
  iduration : "00:11:15"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE169
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/169linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/169linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #179 [Linux Connexion con Pedro Mosquetero Web](https://podcastlinux.com/PL179)
+ Siguiente episodio: Podcasting 2.0
+ Gracias a [Megalodon](https://mastodon.social/@podcastlinux/110099025399673377), una app de Android de Mastodon, puedo programar toots con imágenes
+ [Send](https://mastodon.social/@podcastlinux/110110398375351486), un fork de Firefox Send para enviar archivos pesados
+ [KeppassXC](https://mastodon.social/@podcastlinux/110122618070873875) + Nextcloud para administrar contraseñas en todos mis dispositivos
+ Monitoriza tu gráfica desde la terminal con [nvtop](https://mastodon.social/@podcastlinux/110128280558384202)
+ Utiliza el comando ["sudo dmidecode -t 17"](https://mastodon.social/@podcastlinux/110138661323639558) para saber todo sobre tus módulos de memoria
+ 16 GB más, [32GB en total](https://mastodon.social/@podcastlinux/110149986036364918), para mi AMD de sobremesa
+ Instalado el [SSD NvME Crucial de 1 TB](https://mastodon.social/@podcastlinux/110162387886038231)
+ Arch Linux en el [Yepo](https://mastodon.social/@podcastlinux/110167916892701098)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13  de mayo
+ Evento [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
