---
title: "#33 Linux Express"
date: 2018-01-24
author: Juan Febles
categories: [linuxexpress]
img: 2018/33linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/33linuxexpress
  olength : 2496403
  mlength : 3093393
  iduration : "00:06:26"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE33
---
Un nuevo Linux Express, los audios de Telegram que ya son podcast porque tienen feed.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/33linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/33linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en estas 2 semanas:  
+ [Episodio #43 Especial UlraMoove](http://avpodcast.net/podcastlinux/ultramoove)
+ Sorteo 5 productos [Vant](https://twitter.com/podcastlinux/status/953506421555056641)
+ Próximo episodio, Linux Connexion con [Vant](http://www.vantpc.es)
+ Ya tenemos dominio [podcastlinux.com](https://podcastlinux.com) gracias a [Hefistion](https://twitter.com/Hefistion_)
+ Colabora con [#CadenaDeFavoresWeb](https://twitter.com/hashtag/CadenaDeFavoresweb) de [Mosquetero Web](https://twitter.com/mosqueteroweb)
+ Cacharreando con [OpenMediaVault](http://www.openmediavault.org/)


Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
