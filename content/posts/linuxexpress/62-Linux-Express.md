---
title: "#62 Linux Express"
date: 2019-03-06
author: Juan Febles
categories: [linuxexpress]
img: 2019/62linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/62linuxexpress
  olength : 3957248
  mlength : 4720640
  iduration : "00:09:39"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE62
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/62linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/62linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#72 Especial Vant MiniMOOVE](https://avpodcast.net/podcastlinux/minimoove)
+ Próximo episodio: Audio y GNU/Linux
+ Nuevos dispositivos a analizar
+ Idea de proyecto: Montar PC reciclado
+ Sobre SGAE, Ivoox, Itunes
+ Taller Audacity en mi colegio.
+ Territorio f-Droid: [Activity Diary](https://f-droid.org/es/packages/de.rampro.activitydiary/)
+ [FLISol 19](https://flisol.info/FLISOL2019) Tenerife


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-2320131).

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
