---
title: "#139 Linux Express"
date: 2022-02-16
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/139linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5540096
  mlength : 5248355
  iduration : "00:10:55"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE139
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/139linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/139linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #149 [Hardware Slimbook Executive](https://podcastlinux.com/PL149)
+ Siguiente episodio: [Linux Connexion con Slimbook](https://slimbook.es/executive)
+ [Teclado RGB Slimbook](https://slimbook.es/accesorios-capturadora-4k-teclado-rgb-hub-hdmi-mini-docking#keyboard)
+ [Capturadora 4K de Slimbook](https://slimbook.es/accesorios-capturadora-4k-teclado-rgb-hub-hdmi-mini-docking)
+ Nuevo [Plasma 5.24](https://kde.org/es/announcements/plasma/5/5.24.0)
+ No me [arranca mi ordenador](https://twitter.com/podcastlinux/status/1492238861031940096)
+ Nuevo extra en el [Curso Kdenlive](https://podcastlinux.com/kdenlive)
+ Cierra [DevTube](https://devtube.dev-wiki.de/)
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
