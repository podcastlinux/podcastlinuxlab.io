---
title: "#160 Linux Express"
date: 2022-12-07
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/160linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5473167
  mlength : 5108339
  iduration : "00:10:38"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE160
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/160linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/160linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #169 [ Linux Connexion con Pedro MosqueteroWeb](https://podcastlinux.com/PL170)
+ Siguiente episodio: [Godot](https://godotengine.org), motor libre de videojuegos
+ Próximo proyecto: PodLi, the Podcast Linux Game
+ ¿Cómo [quemas tus distros](https://nitter.net/podcastlinux/status/1595650921882591233#m) en un pendrive?
+ Probando [Arch Linux](https://nitter.net/podcastlinux/status/1597462862276038658#m)
+ [Encuesta](https://nitter.net/podcastlinux/status/1598188142842220545#m) sobre la distro no debianita preferida
+ Campaña de recaudación [#BlueFriday](https://nitter.net/podcastlinux/status/1598926702758936577#m) de KDE
+ Súmate a [#ViernesDeEscritorio](https://twitter.com/hashtag/ViernesDeEscritorio)
+ ¿Twitter o Mastodon?
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
