---
title: "#127 Linux Express"
date: 2021-09-01
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/127linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5002665
  mlength : 4986713
  iduration : "00:10:23"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE127
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/127linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/127linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #136 [Linux Connexion con Luis Fajardo](https://podcastlinux.com/PL137)
+ Siguiente episodio: Hardware [Slimbook One AMD](https://slimbook.es/one)
+ Vídeo análisis del [Slimbook One AMD](https://youtu.be/5z-ANdecbqk)
+ Curso sobre [Kdenlive](https://podcastlinux.com/kdenlive)
+ Vuelta a las [actualizaciones manuales](https://twitter.com/podcastlinux/status/1430816473971298308) en KDE Neon
+ Conectando [Hercules Dj Control Instinct](https://twitter.com/podcastlinux/status/1431586928114216960) a [Mixxx](https://mixxx.org)
+ Podcast [24H24L](https://24h24l.org/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
