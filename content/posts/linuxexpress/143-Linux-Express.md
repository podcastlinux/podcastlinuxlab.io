---
title: "#143 Linux Express"
date: 2022-04-13
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/143linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5505664
  mlength : 5272179
  iduration : "00:10:58"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE143
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/143linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/143linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #153 [Linux Connexion con Atareao](https://podcastlinux.com/PL153)
+ Siguiente episodio: Especial [FLISol Tenerife22](https://flisol.info/FLISOL2022/Espana/Tenerife)
+ [Slimbook One AMD Ryzen 9](https://slimbook.es/one)
+ Actualizando equipos en el cole con [LMDE 5](https://twitter.com/podcastlinux/status/1511998859056197632)
+ Montando un [amplificardor sencillo](https://twitter.com/podcastlinux/status/1509390179378741249)
+ Reciclando [baterías 18650](https://twitter.com/podcastlinux/status/1512650431180201985) para proyectos
+ Amplificador Aliexpress [ZK-502H](https://twitter.com/podcastlinux/status/1511194278155935744)
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
