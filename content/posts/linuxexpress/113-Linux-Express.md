---
title: "#113 Linux Express"
date: 2021-02-17
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/113linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6183894
  mlength : 5955124
  iduration : "00:12:24"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE113
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/113linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/113linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #123 [Linux Connexion con Germán Martín](https://podcastlinux.com/PL123)
+ Siguiente episodio: Git
+ Próximos episodios
+ Sustituye tu router con [Naseros](https://youtu.be/vHAAn2NpTFo)
+ Episodio [KDE España](https://youtu.be/vbA8eWETKIE) sobre GNUHealth
+ Yoyo nos muestra su instalación de [Fedora](https://youtu.be/z3bGbsjWtWc)
+ Podcast de [Davidochobits](https://www.ivoox.com/podcast-davidochobits-podcast_sq_f1260757_1.html)
+ [Setup de escritorio de Voro](https://youtu.be/CWMqSVudTf4)
+ Evento online [Open Data Day A Coruña](https://odd2021acoruna.gitlab.io/)
+ Buscando apoyos para [FLISOL Tenerife](https://t.me/flisoltenerife19) online 2021.

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
