---
title: "#159 Linux Express"
date: 2022-11-23
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/159linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5147669
  mlength : 4919839
  iduration : "00:10:14"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE159
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/159linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/159linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #169 [Ajedrez y Software Libre](https://podcastlinux.com/PL169)
+ Siguiente episodio: Linux Connexion con Pedro MosqueteroWeb
+ Jugando en [Lichess](https://nitter.net/podcastlinux/status/1590577492070305792#m)
+ Actualizar [KDEneon](https://nitter.net/podcastlinux/status/1593114208060612608#m)
+ Altavoces para el [#PCRecicladoDeLaAudiencia](https://nitter.net/podcastlinux/status/1593895242683514880#m)
+ ¿Y si pruebo [Arch](https://archlinux.org)?
+ Masivas altas en [Mastodon](https://joinmastodon.org)
+ ¿[Raspberry Pi Pico](https://nitter.net/podcastlinux/status/1591362665791688705#m) falsas?
+ Configuración personal de los paneles en [Kdenlive](https://nitter.net/podcastlinux/status/1592389431729672192#m)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
