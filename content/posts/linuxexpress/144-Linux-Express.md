---
title: "#144 Linux Express"
date: 2022-04-27
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/144linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5957889
  mlength : 5762863
  iduration : "00:12:00"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE144
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/144linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/144linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #154 [Especial FLISol Tenerife 2022](https://podcastlinux.com/PL154)
+ Siguiente episodio: ESP32
+ Probando [Platformio](https://twitter.com/podcastlinux/status/1516008682395422721?s=20&t=V8rZYw0v87Hua9MyRlB6Kw) para programar la ESP32
+ Haciendo música con [LMMS](https://twitter.com/podcastlinux/status/1516492258916376578)
+ Un amplificador Chino Hi-Res [192Khz HiFi Amlifier](https://twitter.com/podcastlinux/status/1516278627515699203)
+ En vacaciones con mi [ThinkPad X260](https://twitter.com/podcastlinux/status/1514529240145932295?s=20&t=V8rZYw0v87Hua9MyRlB6Kw)
+ Probando la apk ofifial de [Mastodon](https://twitter.com/podcastlinux/status/1517918100565499905)
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
