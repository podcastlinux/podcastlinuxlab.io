---
title: "#115 Linux Express"
date: 2021-03-17
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/115linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5407632
  mlength : 5418256
  iduration : "00:11:17"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE115
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/115linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/115linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #125 [Linux Connexion con Almudena García](https://podcastlinux.com/PL125)
+ Siguiente episodio: PYMES y Software Libre
+ Evento online [Open Data Day A Coruña](https://odd2021acoruna.gitlab.io/)
+ Directo [24H24L](https://devtube.dev-wiki.de/videos/watch/707eb6ff-d42d-4693-b9d5-0d5303843a10) sobre el futuro de GNU/Linux
+ Desempolvando mi [Thinkpad x220](https://mastodon.social/@podcastlinux/105875282007310480)
+ Nuevo Thinkpad gracias a Almudena García y José Jiménez
+ Emisiones en directo con [Peertube](https://devtube.dev-wiki.de/accounts/podcastlinux/video-channels)
+ Eventos a tener en cuenta: [1ª Cumbre Software Libre y Educación](https://gnulinuxvalencia.org/1a-cumbre-software-libre-y-educacion/), [Maratón Pod](http://maratonpod.com), [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)
+ Concretando en la [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
