---
title: "#103 Linux Express"
date: 2020-09-30
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/103linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5588028
  mlength : 5305407
  iduration : "00:11:02"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE103
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/103linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/103linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #113 [Solucionando problemas con GNU/Linux](https://podcastlinux.com/PL113)
+ Siguiente episodio: Linux Connexion con [Pásate a GNU/Linux](https://t.me/pasategnulinux)
+ Pronto llegará [Slimbook Essential](https://slimbook.es/essential)
+ Modificando mkv con [MkvTollnix](https://mkvtoolnix.download/)
+ 500 seguidores en [Mastodon](https://mastodon.social/@podcastlinux)
+ Cierra Send Firefox. Alternativa: [Upload.disroot.org](https://upload.disroot.org/)
+ No dejes de ver el documental [El dilema de las redes](https://www.netflix.com/es/title/81254224)
+ Evento [24H24L](https://www.24h24l.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
