---
title: "#30 Linux Express"
date: 2017-12-13
author: Juan Febles
categories: [linuxexpress]
img: 2017/30linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/30linuxexpress
  olength : 3560877
  mlength : 3846456
  iduration : "00:09:09"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE30
---
Todos los miércoles, entrega de un podcast, ya sea Linux Express o Podcast Linux. Hoy tienes un Linux Express.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/30linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/30linuxexpress.mp3" type="audio/mpeg">
</audio>

Último mes del año con muchas cosas en el asador:

+ [Episodio #40 Linux Connexion con Aleix Pol](http://avpodcast.net/podcastlinux/aleixpol).
+ Próximo episodio Gaming y GNU/Linux
+ Se acerca un episodio especial. ¿Qué será?
+ Audios [KillAll Radio Podcast Telegram](https://t.me/killallradiopodcast)
+ [Gpodder](https://gpodder.github.io/) y [Gpodder.net](https://gpodder.net/)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
