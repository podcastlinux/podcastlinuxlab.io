---
title: "#164 Linux Express"
date: 2023-02-01
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/164linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6542401
  mlength : 5926811
  iduration : "00:12:05"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE164
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/164linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/164linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #174 [Especial Distros no Debianitas](https://podcastlinux.com/PL174)
+ Siguiente episodio:Linux Connexion Distros no Debianitas
+ Repositorios que utilizo en [pacman](https://nitter.net/podcastlinux/status/1614585832265981954#m)
+ [Backup](https://nitter.net/podcastlinux/status/1615219866553749505#m) de paquetes pacman instalados
+ [Script](https://gitlab.com/podcastlinux/scripts/-/blob/master/PostInstalaArch.sh) postinstalación de Arch Linux
+ Utilizando [qpwgraph y JackMixer](https://nitter.net/podcastlinux/status/1615944642322698240#m) para grabar
+ No me he podido resistir a conseguir una [camiseta Arch Linux](https://nitter.net/podcastlinux/status/1617092332532244481#m)
+ Muy contento con el [kit Xeon chino](https://nitter.net/podcastlinux/status/1617756709577015297#m) y Arch Linux
+ Probando [CrowTranslate](https://nitter.net/podcastlinux/status/1616729817432395776#m), un traductor para tu escritorio con muchas posibilidades
+ Probando [Whisper](https://nitter.net/podcastlinux/status/1618481358938017793#m), una inteligencia artificial libre para transcribir audio
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
