---
title: "#151 Linux Express"
date: 2022-08-03
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/151linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6778583
  mlength : 6398579
  iduration : "00:13:19"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE151
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/151linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/151linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #161 [Linux Connexion con Baltasar Ortega](https://podcastlinux.com/PL161)
+ Siguiente episodio: Linux Connexion con Juan Antonio González
+ Usando [Pipeware](https://nitter.net/podcastlinux/status/1550016082383101954#m) en mi portátil
+ Peertube en tu Android con [TubeLab](https://nitter.net/podcastlinux/status/1551168687612248064#m)
+ Jugando a [Stevedore](https://nitter.net/podcastlinux/status/1551834093305925633#m)
+ Terminada la intro para la 7ª Temporada
+ Música con LMMS para audios de [Libre Home Gym](https://nitter.net/podcastlinux/status/1552568988147847169#m)
+ [Jpod](https://jpod.es) en Madrid 
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy](https://akademy.kde.org/2022) en Barcelona en octubre 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
