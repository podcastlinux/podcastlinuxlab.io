---
title: "#36 Linux Express"
date: 2018-03-07
author: Juan Febles
categories: [linuxexpress]
img: 2018/36linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/36linuxexpress
  olength : 3904488
  mlength : 4577280
  iduration : "00:08:48"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE36
---
Un nuevo Linux Express, los audios de Telegram que se intercalan con los formales.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/36linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/36linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #46 Linux Express](http://avpodcast.net/podcastlinux/galpon)
+ Siguiente episodio, Especial [YEPO737A](https://twitter.com/hashtag/YEPO737A).
+ Imágenes y vídeos libres en [Pixabay](https://pixabay.com/).
+ Gran respuesta de [#EscritorioGNULinux](https://twitter.com/hashtag/escritoriognulinux).
+ Distros que nunca he probado.
+ 31 de marzo: [Maratón Linuxero](https://maratonlinuxero.org/).

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-2298286/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
