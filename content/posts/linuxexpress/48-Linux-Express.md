---
title: "#48 Linux Express"
date: 2018-08-20
author: Juan Febles
categories: [linuxexpress]
img: 2018/48linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/48linuxexpress
  olength : 5684921
  mlength : 7555072
  iduration : "00:15:28"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE48
---
El verano me deja el suficiente tiempo libre para trastear e indagar.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/48linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/48linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio #58 Linux Connexion con Dabo](https://avpodcast.net/podcastlinux/dabo)
+ Próximo episodio: Linux Connexion con [Manz](https://emezeta.com/)
+ Viaje a Asturias desvirtualizando [Linuxeros](https://twitter.com/podcastlinux/status/1029650020918865925)
+ Utilizando [Maps](https://f-droid.org/en/packages/com.github.axet.maps/) en mis viajes.
+ Territorio f-Droid: [Orgzly](https://f-droid.org/en/packages/com.github.axet.maps/)
+ Comparte tu primer GNU/Linux con [#MiPrimerGNULinux](https://twitter.com/hashtag/miprimergnulinux)
+ Acer Aspire E11 con [KDENeon](https://neon.kde.org)
+ Vídeos de [Odroid Go](https://www.hardkernel.com/main/products/prdt_info.php?g_code=G152875062626): Consola portátil retrogaming
+ [Maraton Linuxero 1 de septiembre](https://maratonlinuxero.org)

Las imágenes utilizadas están bajo [licencia Creative Commons](https://creativecommons.org) y forma parte de [Odroid](https://www.hardkernel.com) y [Mojon Twins](http://www.mojontwins.com/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
