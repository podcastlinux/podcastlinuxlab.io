---
title: "#184 Linux Express"
date: 2023-11-08
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/184linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6376673
  mlength : 6021782
  iduration : "00:12:24"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE184
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/184linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/184linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #194 [Distros Madres II](https://podcastlinux.com/PL194)
+ Episodio #195 Linux Connexion Distros Madres II
+ Error en las [actualizaciones pip](https://mastodon.social/@podcastlinux/111288110455021197)
+ [Kdenlive](https://mastodon.social/@podcastlinux/111299435688179428) mi elección como editor de vídeo
+ [Fasfetch](https://mastodon.social/@podcastlinux/111311703394597311) para mostrar mi informmación en la terminal
+ [ksudoku](https://mastodon.social/@podcastlinux/111317366446356886) para realizar sudokus
+ Conozco [TuxMath](https://mastodon.social/@podcastlinux/111327982542861626) gracias a un oyente
+ Alternativas a [JClic](https://mastodon.social/@podcastlinux/111339307247392109)
+ Quema tu ISO desde la terminal con [dd](https://mastodon.social/@podcastlinux/111351575356151309)
+ [DuckDuckGo](https://mastodon.social/deck/@podcastlinux/111357237707106329), mi buscador en internet

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
