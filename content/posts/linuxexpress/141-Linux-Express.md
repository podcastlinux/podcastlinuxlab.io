---
title: "#141 Linux Express"
date: 2022-03-16
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/141linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4221144
  mlength : 4070755
  iduration : "00:08:28"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE141
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/141linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/141linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #151 [Linux Connexion con David Vargas](https://podcastlinux.com/PL151)
+ Siguiente episodio: Paqueterías Multidistribución GNU/Linux
+ Transformar [HEIC en jpg](https://twitter.com/podcastlinux/status/1501904570729041921) en GNU/Linux
+ Activar [pipeware](https://twitter.com/podcastlinux/status/1502554993869484034) en KDE Neon
+ Episodio de [Mancomún Podcast](https://www.mancomun.gal/es/documento/mancomun-podcast-repositorio-dos-programas) sobre Hardware Libre
+ [Arduino Week](http://week.arduino.cc/events) el 26 de marzo
+ [Flisol 2022](https://flisol.info/) el 23 de abril
+ [esLibre](https://eslib.re/2022) presencial en Vigo el 24 y 25 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
