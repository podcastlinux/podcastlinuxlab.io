---
title: "#183 Linux Express"
date: 2023-10-25
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/183linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6516657
  mlength : 6065041
  iduration : "00:12:29"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE183
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/183linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/183linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #193 [Linux Connexion con Eduardo Medina](https://podcastlinux.com/PL193)
+ Episodio #194 Distros Madres II
+ Alumnado de 1º ESO realiza un trabajo sobre [Linus Torvalds](https://mastodon.social/@podcastlinux/111208837959542152)
+ Un año jugando al ajedrez en [Lichess](https://mastodon.social/@podcastlinux/111220162650153879)
+ Mis fondos IA de [#ViernesDeEscritorio](https://mastodon.social/@podcastlinux/111232430956282249)
+ Gracias a un oyente conozco [Xournal++](https://mastodon.social/@podcastlinux/111238093264592227)
+ Nueva versión [Ardour 8.0](https://mastodon.social/@podcastlinux/111248474453007614)
+ Fallo al arrancar con [dos monitores](https://mastodon.social/@podcastlinux/111259798925401079)
+ Nace [#SábadoDeTerminal](https://mastodon.social/@podcastlinux/111272067085579371)
+ [Kate](https://mastodon.social/@podcastlinux/111277729497141716) como editor de texto plano

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
