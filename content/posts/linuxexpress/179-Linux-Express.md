---
title: "#179 Linux Express"
date: 2023-08-30
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/179linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5613490
  mlength : 5392127
  iduration : "00:11:05"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE179
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/179linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/179linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #189 [Linux Connexion con Sr. Kenobi](https://podcastlinux.com/PL189)
+ Episodio #190 Hardware [Miyoo Mini Plus](https://miyoominiv2.com/miyoo-mini-plus/)
+ Encuesta [tema claro o tema oscuro](https://mastodon.social/@podcastlinux/110891748568180296)
+ [OpenTracks](https://mastodon.social/@podcastlinux/110903073646523108) para grabar y gestionar tus rutas al aire libre
+ Vídeos de las charlas [Akademyes23](https://mastodon.social/@podcastlinux/110915341614526463)
+ 30 años de [Debian](https://mastodon.social/@podcastlinux/110921004503678084)
+ Charla del gran [David Marzal](https://mastodon.social/@podcastlinux/110931385275314096) sobre transcripción automática de audio
+ Utilizar el editor de [Lichess](https://mastodon.social/@podcastlinux/110942709597450866) para practicar posiciones y finales
+ [Offline Chess Puzzles](https://mastodon.social/@podcastlinux/110954977744616790) para realizar ejercicios de Lichess en local
+ [Seamless](https://mastodon.social/@podcastlinux/110960640548178778), IA de Meta Open Source para transcribir y reproducir voz en otros idiomas


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
