---
title: "#82 Linux Express"
date: 2019-12-11
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/82linuxexpress
  olength : 4664550
  mlength : 5847040
  iduration : "00:11:55"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE82
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/82linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/82linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #92[Productividad en GNU/Linux](https://avpodcast.net/podcastlinux/productividad)
+ Próximo episodio: Productividad en GNU/Linux 2
+ [Kit básico de Arduino](https://hackmd.io/@podcastlinux/HyaG97oqB)
+ [Imágenes de componentes](https://archive.org/details/kitarduino) para tutoriales de Arduino con [Fritzing](https://fritzing.org/home/)
+ Organizando material didáctico para Arduino.
+ Pendiente el curso de creación web estática con [Hugo](https://gohugo.io/) y [Gitlab](https://gitlab.com/pages/hugo)
+ Cierra la [OSL ULL](https://www.ull.es/servicios/osl/), por el momento.
+ Nuevo logo de [AvPodcast](https://avpodcast.net/) y más cambios.


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
