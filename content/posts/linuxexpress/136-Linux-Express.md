---
title: "#136 Linux Express"
date: 2022-01-05
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/136linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6504009
  mlength : 6020744
  iduration : "00:12:32"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE136
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/136linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/136linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #146 [Hardware Vant Agile](https://podcastlinux.com/PL146)
+ Siguiente episodio: Proyectos Podcast Linux
+ Consolas de 2ª mano para instalarle RetroArch
+ La Navidad: Momentos Tux
+ [Proyecto Libre Home Gym](https://gitlab.com/podcastlinux/librehomegym) para realizar ejercicios en casa
+ Episodio [Mahjong en 10 minutos](https://www.ivoox.com/m10m-seguimos-linux-audios-mp3_rf_78854224_1.html) sobre Linux
+ Episodio Publicidad en Windows de [Gabriel Viso](https://anchor.fm/gvisoc/episodes/Microsoft-y-tu-atencin-e1alt76/a-a6v0d2d)
+ Proyecto [Los últimos de FEEDlipinas](https://twitter.com/ultimosfeed)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
