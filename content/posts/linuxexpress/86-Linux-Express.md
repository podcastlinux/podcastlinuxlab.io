---
title: "#86 Linux Express"
date: 2020-02-05
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/86linuxexpress
  olength : 4299950
  mlength : 5175296
  iduration : "00:10:31"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE86
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/86linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/86linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #96 [Linux Connexion con Álvaro Nova](https://avpodcast.net/podcastlinux/alvaronova2)
+ Próximo episodio:Vídeo y GNU/Linux
+ Curso [Ardour](https://www.youtube.com/user/GDFestudio) de [José GDF](https://twitter.com/JoseGDF)
+ Nuevo [LibreOffice 6.4](https://es.blog.documentfoundation.org/libreoffice-6-4/)
+ Nueva versión [AntennaPod 1.8.0](https://twitter.com/antennapod/status/1221402423790637056)
+ Nuevo [AIO](https://twitter.com/vantpc/status/1222555532382351361) de [Vant](https://www.vantpc.es/)
+ Formato para el episodio 100

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
