---
title: "#55 Linux Express"
date: 2018-11-28
author: Juan Febles
categories: [linuxexpress]
img: 2018/55linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/55linuxexpress
  olength : 3470727
  mlength : 4255744
  iduration : "00:08:39"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE55
---
Agradecer de nuevo a las empresas colaboradoras del programa: [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://archive.org/download/linuxexpress/55linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/55linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:
+ Episodio [#65 Mesa Redonda Pásate a GNU/Linux](https://avpodcast.net/podcastlinux/mesaredonda)
+ Próximo episodio: Especial [Pinebook KDENeon](https://www.pine64.org/?product=11-6-pinebook)
+ [Unboxing Pinebook](https://youtu.be/ZNLQc39baFI)
+ Busco [BQ Aquaris M10 FHD (ubports)](https://devices.ubuntu-touch.io/device/frieza)
+ Sigo con mi [Huawei GX8](https://forum.xda-developers.com/gx8/development/rom-lineageos-14-1-t3596855)
+ Grupo Telegram [@pasategnulinux](https://t.me/pasategnulinux)
+ Territorio f-Droid: [NewPipe](https://f-droid.org/es/packages/org.schabi.newpipe)
+ Visita a Valencia en diciembre.
+ [Maratón Linuxero](https://maratonlinuxero.org/) el 15 de diciembre

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-2364350/). Los logos WUG y Waterdrop Hydroprint son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>
Mastodon: <https://mastodon.social/@podcastlinux/>
Correo: <podcastlinux@avpodcast.net>
Web: <http://avpodcast.net/podcastlinux/>
Blog: <https://podcastlinux.com/>
Telegram: <https://t.me/podcastlinux>
Youtube: <https://www.youtube.com/PodcastLinux>
Feed Podcast Linux: <https://podcastlinux.com/feed>
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>