---
title: "#170 Linux Express"
date: 2023-04-26
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/170linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5846249
  mlength : 5435385
  iduration : "00:11:10"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE170
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/170linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/170linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #180 [Podcasting 2.0](https://podcastlinux.com/PL180)
+ Siguiente episodio: Linux Connexion con [Roberto Ruisánchez](https://tkz.one/@ruisan)
+ Probando la nueva interfaz de [AntennaPod 3.0](https://mastodon.social/@podcastlinux/110178297480292058)
+ Probando [Podverse](https://mastodon.social/@podcastlinux/110189622411378433), un gestor de podcast #SoftwareLibre multiplataforma compatible con el podcasting 2.0
+ Episodio [Sobre La Marcha](https://podcasters.spotify.com/pod/show/gvisoc) de [Gabriel Viso](https://fedi.gvisoc.com/@gabriel) para empezar GNU/Linux con buen pie
+ Estuve en [Las Charlas de Salmorejo Geek](https://youtu.be/swIVgr9WRu8) invitado por [yoyo308](https://mastodon.online/@yoyo308)
+ Una alumna en prácticas utiliza [GNU/Linux en su portátil](https://mastodon.social/@podcastlinux/110217934414989277)
+ Utilizando el [DNIe en Arch Linux](https://mastodon.social/@podcastlinux/110230961523611372)
+ Elegir una [opción por defecto](https://mastodon.social/@podcastlinux/110247188981316621), desde la terminal
+ Gira tus vídeos con [ffmpeg](https://mastodon.social/@podcastlinux/110241526723117275)
+ Evento [FliSol Valencia](https://gnulinuxvalencia.org/sabado-29-de-abril-gnu-linux-valencia-organiza-el-flisol-2023/) el sábado 29 de abril
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13  de mayo
+ Evento [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio



Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
