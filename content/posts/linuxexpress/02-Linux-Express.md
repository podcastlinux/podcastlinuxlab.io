---
title: "#02 Linux Express"
date: 2016-12-15
author: Juan Febles
categories: [linuxexpress]
img: 2017/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/02linuxexpress
  olength : 2240303
  mlength : 2026974
  iduration : "00:04:13"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE02
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/02linuxexpress.mp3" type="audio/mpeg">
</audio>

El 2º linux Express, los audios que comparto cada 2 semanas en [Telegram](https://t.me/podcastlinux) para ir alternando
los podcasts quincenales con éstos.
Aquí encontrarás información de lo que estoy proyectando y realizando mientras esperas a un nuevo podcast.
