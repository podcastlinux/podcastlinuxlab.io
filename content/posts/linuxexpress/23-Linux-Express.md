---
title: "#23 Linux Express"
date: 2017-09-06
categories: [linuxexpress]
author: Juan Febles
img: 2017/23LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/23linuxexpress
  olength : 3283443
  mlength : 3740921
  iduration : "00:06:14"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE23
---
Llega septiembre y entramos en la rutina, tan necesaria a veces para mantaner los buenos hábitos.  

<audio controls>
  <source src="https://archive.org/download/linuxexpress/23linuxexpress.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #33 1º Directo Podcast Linux](http://avpodcast.net/podcastlinux/directo) <https://youtu.be/tItznvHFKFo>
+ Próximo episodio [Directo Podcast Linux en el Maratón Linuxero](https://youtu.be/Yv90j2HVg1Q)
+ Siguiente episodio será sobre formatos libres.
+ Toda la info sobre Maratón Linuxero: <https://maratonlinuxero.org> Telegram: <https://t.me/maratonlinuxero>
+ Idea de realizar otro directo mediante Mixxx y Icecast.

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
