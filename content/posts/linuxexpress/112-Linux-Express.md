---
title: "#112 Linux Express"
date: 2021-02-03
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/112linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4916972
  mlength : 4838128
  iduration : "00:10:04"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE112
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/112linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/112linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #122 [ESP8266](https://podcastlinux.com/PL122)
+ Siguiente episodio: Linux Connexion con [Germán Martín](https://twitter.com/gmag12)
+ [Raspberry Pi Pico](https://www.raspberrypi.org/products/raspberry-pi-pico/)
+ Reutilizando [cargadores viejos](https://twitter.com/podcastlinux/status/1351414533072551936)
+ Instalan ubuntu en un [Apple M1](https://www.omgubuntu.co.uk/2021/01/see-ubuntu-running-on-apple-m1-mac-mini)
+ Mensajería libre: XMPP
+ Nuevo [Vant Ryzen](https://twitter.com/vantpc/status/1354002410839146497)
+ Evento online [Open Data Day A Coruña](https://odd2021acoruna.gitlab.io/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
