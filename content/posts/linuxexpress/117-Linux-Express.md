---
title: "#117 Linux Express"
date: 2021-04-14
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/117linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6474309
  mlength : 6314987
  iduration : "00:13:09"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE117
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/117linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/117linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #127 [Linux Connexion con Francisco Javier Miró](https://podcastlinux.com/PL127)
+ Siguiente episodio: Especial FLISoL Tenerife 2021
+ Colaboraciones directos vídeos en [VoroMV](https://youtu.be/yyp-yfqq3P0) y [Salmorejo Geek](https://www.youtube.com/user/YoyoFernandez)
+ Algunos cambios en el Thinkpad T440P
+ Buscando un Thinkpad x230/x240116linuxexpress
+ Probando XMPP con mi cuenta: podcastlinux@xabber.org
+ Solucionado el feed en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html)
+ Nuevo [teclado mecánico de Slimbook](https://slimbook.es/pedidos/ratones-teclados/teclado-rgb-slimbook-comprar)
+ Nuevos [Vant Moove 2 con Ryzen](https://www.vantpc.es/moove2-amd)
+ Eventos a tener en cuenta: [Maratón Pod](http://maratonpod.com), [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)
+ Avanzando en la [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
