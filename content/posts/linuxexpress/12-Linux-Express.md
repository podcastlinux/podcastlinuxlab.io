---
title: "#12 Linux Express"
author: Juan Febles
date: 2017-05-03
categories: [linuxexpress]
img: 2017/12LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/12linuxexpress
  olength : 4312760
  mlength : 4017498
  iduration : "00:08:22"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE12
---
Aquí tienes la duodécima entrega de Linux Express, el podcast creado de los audios de Telegram, para que estés informado de lo que se cuece en Podcast Linux.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/12linuxexpress.mp3" type="audio/mpeg">
</audio>

Hoy quiero compartir contigo estos temas:

+ [Episodio #23 GNU/Linux y la Terminal](http://avpodcast.net/podcastlinux/terminal)
+ [Próximo episodio con DavidOchoBits](https://www.ochobitshacenunbyte.com)
+ [Blog personal del podcast](https://podcastlinux.github.io)
+ [Feed Linux Express](https://podcastlinux.github.io/Linux-Express/feed)
+ Rondando un nuevo Especial Podcast Linux


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
