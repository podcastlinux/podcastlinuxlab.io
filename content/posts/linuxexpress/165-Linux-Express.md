---
title: "#165 Linux Express"
date: 2023-02-15
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/165linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5157848
  mlength : 4872300
  iduration : "00:09:53"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE165
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/165linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/165linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #174 [Linux Connexion Distros no Debianitas](https://podcastlinux.com/PL175)
+ Siguiente episodio: Hardware [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/)
+ Utilizando [Thonny y MicroPython](https://nitter.net/podcastlinux/status/1622830012876353536#m) con la Pi Pico
+ [Wokwi](https://nitter.net/podcastlinux/status/1624279563433046017#m), ¿un simulador libre para la Raspberry Pi Pico?
+ Probando [Megalodon](https://nitter.net/podcastlinux/status/1619640343494033410#m), un cliente libre de Mastodon para tu Android
+ ¿A veces los enlaces de [Nitter](https://nitter.net/podcastlinux/status/1620293296798027776#m) te dan error?
+ Cambio de batería del [Thinkpad X260](https://nitter.net/podcastlinux/status/1621018073846226944#m)
+ Instalar Arch Linux con [conexión Wifi](https://nitter.net/podcastlinux/status/1621803247500410880#m)
+ Visita al colegio de [Estrellas Compartidas](https://nitter.net/podcastlinux/status/1623554787747762176#m)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 12 y 13 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
