---
title: "#70 Linux Express"
date: 2019-06-26
author: Juan Febles
categories: [linuxexpress]
img: 2019/70linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/70linuxexpress
  olength : 3399331
  mlength : 4065280
  iduration : "00:08:18"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE70
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/70linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/70linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#80 Especial Vant Life A](https://avpodcast.net/podcastlinux/vantlifea)
+ Próximo episodio: Especial 3º Aniversario
+ Episodios de verano
+ Desembarco Slimbook:[Pro](https://slimbook.es/pro-ultrabook-13-aluminio), [Pro X](https://slimbook.es/prox) y [Apollo](https://slimbook.es/pedidos/slimbook-apollo)
+ [Atareao](https://www.pine64.org/pinephone/) gana [Mejor Medio Open Awards 2019](https://openexpoeurope.com/es/oe2019/open-awards-2019/)
+ Nueva actualización de [ShotCut 19.06.15](https://www.shotcut.org/blog/new-release-190615/)
+ Futuros eventos: [PodcastDay](https://www.verkami.com/projects/23539-podcast-days-2019-madrid) y [MaratónPod](https://t.me/maratonpod)
+ Territorio f-Droid: [OpenArchive](https://f-droid.org/es/packages/net.opendasharchive.openarchive.release/)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-3218996/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
