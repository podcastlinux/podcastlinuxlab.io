---
title: "#43 Linux Express"
date: 2018-06-13
author: Juan Febles
categories: [linuxexpress]
img: 2018/43linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/43linuxexpress
  olength : 4658264
  mlength : 5603328
  iduration : "00:11:29"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE43
---
Aquí tienes otro Linux Express, los audios de Telegram que se alternan con Podcast Linux para que sepas lo que se cuece para próximos programas.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/43linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/43linuxexpress.mp3" type="audio/mpeg">
</audio>

Te dejo el cacharreo e información al que he estado dándole vueltas en esta quincena:  
+ [Episodio #53 Libera tu móvil](https://avpodcast.net/podcastlinux/liberatumovil)
+ Siguiente episodio, Linux Connexion con Marcos Costales
+ Premios [Open Awards 18](https://openexpoeurope.com/es/)
+ Episodios para el verano
+ Colabora en el 2º aniversario: Manda tu audio a <https://t.me/juanfebles>
+ Un año con [KDE Neon](https://neon.kde.org/)
+ Territorio f-Droid: [Audio Recorder](https://f-droid.org/en/packages/com.github.axet.audiorecorder/)
+ [Curso Edición de vídeo con Shotcut](https://podcastlinux.com/shotcut)

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-3095916/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
