---
title: "Chicles funcionales WUG"
date: 2018-09-19
author: Juan Febles
categories: [linuxexpress]
img: 2018/wug.png
podcast:
  audio: 
  olength :
  mlength :
  iduration :
tags: [wug]
comments: true
---
![]({{< ABSresource url="/images/2018/wug.png" >}})  
Gracias a ti, la empresa de chicles funcionales [WUG](https://wugum.com/) se ha puesto en contacto conmigo para una colaboración.  

Después de probarlos personalmente y ver que funcionan, puedo recomendar este producto.
He utilizado [Energy +](https://wugum.com/health-energyplus/) para realizar deporte y en el trabajo y [Relax](https://wugum.com/health-relax/) para dormir y te puedo asegurar que se nota.

Su composición es a base de productos vegetales sin gluten, sin lactosa y sin azúcares añadidos.

Para apoyar a Podcast Linux y a la vez beneficiarte de una promoción de 6 chicles, utiliza el Cupón Promocional **podcastlinux**. Con pedidos de 15 € tienes el envío gratuito. Todos los beneficios que obtenga irán a productos GNU/Linux para testearlos. Pronto me llegará uno.

Realiza el pedido aquí: <https://wugum.com/tienda/es/>

![]({{< ABSresource url="/images/2018/wug2.png" >}})  

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
