---
title: "#22 Linux Express"
date: 2017-08-07
categories: [linuxexpress]
author: Juan Febles
img: 2017/22LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/22linuxexpress
  olength : 4577824
  mlength : 8213760
  iduration : "00:08:33"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE22
---
Recta final del mes de agosto. Semanalmente alternamos el podcast normal con este Linux Express y estas semanas han pasado muchas cosas.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/22linuxexpress.mp3" type="audio/mpeg">
</audio>

Recta final del verano:

+ [Episodio #32 Linux Connexion con Reciclanet](https://avpodcast.net/podcastlinux/reciclanet/)
+ Próximo episodio 1º Directo de Podcast Linux. ¿Mixxx + Icecast o OBS Studio + Youtube?
+ Siguiente distro a instalar [Ubuntu Studio](https://ubuntustudio.org/)
+ Último ensayo de [Maratón Linuxero](https://maratonlinuxero.org) con 4 directos y sorteos de productos linuxeros.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
