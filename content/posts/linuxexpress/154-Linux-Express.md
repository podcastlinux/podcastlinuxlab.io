---
title: "#154 Linux Express"
date: 2022-09-14
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/154linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6781026
  mlength : 6654579
  iduration : "00:13:51"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE154
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/154linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/154linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #164 [15 años con GNU/Linux](https://podcastlinux.com/PL164)
+ Siguiente episodio: Anbernic RG350 M/P
+ Primer componente del [PC Reciclado de la Audiencia](https://podcastlinux.com/pcreciclado)
+ Ubuntu 22.10 vendrá con [Pipeware por defecto](https://nitter.net/podcastlinux/status/1565200061457473536#m)
+ Nueva versión de [OBS Studio](https://www.muylinux.com/2022/09/01/obs-studio-28)
+ Horarios del colegio generados con [FET](https://lalescu.ro/liviu/fet/)
+ Probando generar subtítulos automáticos en [Kdenlive](https://nitter.net/podcastlinux/status/1563400451449466881#m)
+ [Mi charla en la [Akademy-es](https://www.kdeblog.com/presenta-tu-charla-a-akademy-es-2022-de-barcelona-akademyes.html)
+ Finalistas en las [Jpod](https://nitter.net/podcastlinux/status/1567007183178321920#m) 
+ [Akademy-es](https://www.kdeblog.com/presentado-akademy-es-2022-en-barcelona-y-en-linea-akademyes.html) y [Akademy Internacional](https://akademy.kde.org/2022) en Barcelona en octubre 

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
