---
title: "#120 Linux Express"
date: 2021-05-26
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/120linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6224284
  mlength : 5937570
  iduration : "00:12:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE120
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/120linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/120linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #130 [Linux Connexion con Bote](https://podcastlinux.com/PL130)
+ Siguiente episodio: Directo con Software Libre
+ Episodios para verano
+ Pipewire en [Ubuntu y derivadas](https://salmorejogeek.com/tag/pipewire/)
+ Dock para mi T440P
+ Nuevo [Vant Moove2-14](https://www.vantpc.es/moove-r2-14)
+ Podcast [Voro MV](https://anchor.fm/voromv)
+ Eventos a tener en cuenta: [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
