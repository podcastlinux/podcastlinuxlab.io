---
title: "#37 Linux Express"
date: 2018-03-21
author: Juan Febles
categories: [linuxexpress]
img: 2018/37linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/37linuxexpress
  olength : 4762891
  mlength : 6187008
  iduration : "00:11:59"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE37
---
Un nuevo Linux Express, los audios de Telegram que se intercalan con los formales.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/37linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/37linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #47 Especial YEPO737A](http://avpodcast.net/podcastlinux/yepo737a)
+ Siguiente episodio, Linux Connexion con [Ubucon Europe 18](http://ubucon.org/en/events/ubucon-europe/)
+ [SSD chino](https://www.gearbest.com/hdd-ssd/pp_1656577.html) pra el YEPO 737A
+ Mi primer [script](https://gitlab.com/podcastlinux/scripts)
+ [KDE Connect](https://github.com/Bajoja/indicator-kdeconnect) en Xfce
+ Nueva [Raspberry Pi 3B+](https://www.raspberrypi.org/blog/raspberry-pi-3-model-bplus-sale-now-35/)
+ Promueve [Contenido Libre](https://search.creativecommons.org/)
+ 31 de marzo: [Maratón Linuxero](https://maratonlinuxero.org/).

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-33838/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
