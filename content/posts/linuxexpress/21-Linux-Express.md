---
title: "#21 Linux Express"
author: Juan Febles
date: 2017-08-07
categories: [linuxexpress]
img: 2017/21LinuxExpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/21linuxexpress
  olength : 4314767
  mlength : 8269824
  iduration : "00:08:36"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE21
---
Seguimos activos en verano. Semanalmente alternamos el podcast normal con este Linux Express y estas semanas han pasado muchas cosas.

<audio controls>
  <source src="https://archive.org/download/linuxexpress/21linuxexpress.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en esta primera semana del mes de agosto:

+ [Episodio #31 Especial TLP2017](https://avpodcast.net/podcastlinux/tlp2017/)
+ Próximo episodio Linux Connexion con [ReciclaNet](http://www.reciclanet.org/)
+ Sigo disfrutando de [Ardour](https://ardour.org/) con mi nueva interfaz de audio [Behringer UMC202HD](https://www.music-group.com/Categories/Behringer/Computer-Audio/Audio-Interfaces/UMC202HD/p/P0BJZ)
+ Apúntate al [Maratón Linuxero](https://t.me/maratonlinuxero)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
