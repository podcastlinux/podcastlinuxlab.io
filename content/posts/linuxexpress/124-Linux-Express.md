---
title: "#124 Linux Express"
date: 2021-07-21
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/124linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6507426
  mlength : 6266086
  iduration : "00:13:03"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE124
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/124linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/124linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #134 [Linux Connexion con David Marzal](https://podcastlinux.com/PL133)
+ Siguiente episodio: Linux Connexion con Erica Aguado
+ [Podcastlinux.com](https://podcastlinux.com) en tema oscuro
+ Atajos de teclado en [KdeNeon](https://twitter.com/kdecommunity/status/1407655669877051396)
+ Utilizando el lanzador [Krunner](https://krunner.rocks/)
+ Cambio del TrackPoint a mi [X260](https://twitter.com/podcastlinux/status/1413787932247330817)
+ Ya tengo el [Slimbook One Ryzen](https://slimbook.es/one)
+ Nueva consola [Steam Deck](https://www.steamdeck.com/es/)
+ Podcast [KDE España](https://www.ivoox.com/podcast-podcast-kde-espana_sq_f1249423_1.html)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
