---
title: "#158 Linux Express"
date: 2022-11-09
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/158linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5442273
  mlength : 5149299
  iduration : "00:10:43"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE158
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/158linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/158linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #168 [Linux Connexion con Iraisy Figueroa](https://podcastlinux.com/PL168)
+ Siguiente episodio: Ajedrez y Software Libre
+ Probando [Scid con StockFish]() para analizar partidas de ajedrez
+ [PocketCast](https://nitter.net/podcastlinux/status/1588040777459912704#m) pasa a ser Software Libre
+ [Placa y Micro](https://nitter.net/podcastlinux/status/1589852717161230337#m) para el [#PCRecicladoDeLaAudiencia](https://podcastlinux.com/pcreciclado)
+ [Trabajo](https://nitter.net/podcastlinux/status/1586213738373296128#m) de alumnos del colegio sobre Linus Torvalds
+ [Actualizando KDE Neon](https://nitter.net/podcastlinux/status/1585488962176491521#m) a la versión 22.04
+ Ya tengo mi Camiseta [Spectrum ZX](https://nitter.net/podcastlinux/status/1589188338732318720#m)
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
