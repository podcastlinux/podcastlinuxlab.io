---
title: "#173 Linux Express"
date: 2023-06-07
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/173linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5271014
  mlength : 4910429
  iduration : "00:10:05"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE173
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/173linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/173linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #182 [Linux Connexion con Yoyo Fernández](https://podcastlinux.com/PL183)
+ Siguiente episodio: [Especial Preguntas y Respuestas](https://mastodon.social/@podcastlinux/110427439240816890)
+ Probando [SyncthingTrayQT6](https://mastodon.social/@podcastlinux/110416114541509525) para notificaciones de este servicio
+ Liberando mi antiguo móvil con [Twrp](https://mastodon.social/@podcastlinux/110439707479910440)
+ Eligiendo [LineageOS](https://mastodon.social/@podcastlinux/110445369781819564) como sistema Android Libre
+ Aplicaciones y servicios Libres para mi [Android](https://mastodon.social/@podcastlinux/110445369781819564)
+ [Blanket](https://mastodon.social/@podcastlinux/110455751336000612), una aplicación libre para generar sonidos ambientes
+ Únete a [#ViernesDeEscritorio](https://mastodon.social/tags/ViernesDeEscritorio)
+ Base para [Almacemaniemto Sata](https://mastodon.social/@podcastlinux/110479343698825774)
+ Nuevo [Micrófono Behringer SB78A](https://mastodon.social/@podcastlinux/110485005998430756)
+ Evento [Akademy-es](https://mastodon.social/@podcastlinux/109982596234913476): Málaga el 9 y 10 de junio

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
