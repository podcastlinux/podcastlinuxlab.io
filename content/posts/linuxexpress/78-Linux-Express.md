---
title: "#78 Linux Express"
date: 2019-10-16
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/78linuxexpress
  olength : 5612188
  mlength : 6825984
  iduration : "00:13:57"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE78
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/78linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/78linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#88 Creative Commons](https://avpodcast.net/podcastlinux/creativecommons)
+ Próximo episodio: Linux Connexion con [Irene Soria](https://twitter.com/arenitasoria)
+ Mis primeros proyectos con [Arduino](https://www.arduino.cc/)
+ Calendario, notas, citas y eventos en tu terminal con [Calcurse](https://www.calcurse.org/)
+ Utilizando [Klavaro](https://klavaro.sourceforge.io/en) en mi colegio para trabajar mecanografía. 
+ Próximos Eventos: [Linux y Tapas](https://linuxytapas.wordpress.com/) y [Maratón Pod](https://www.maratonpod.es/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
