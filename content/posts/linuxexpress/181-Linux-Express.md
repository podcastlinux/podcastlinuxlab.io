---
title: "#181 Linux Express"
date: 2023-09-27
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/181linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 7750966
  mlength : 7351519
  iduration : "00:15:10"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE181
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/181linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/181linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #191 [Linux Connexion  con Ninoh-FOX](https://podcastlinux.com/PL191)
+ Episodio #192 Distros Inmutables
+ [Kernel 6.5](https://mastodon.social/@podcastlinux/111050293662291796) en Arch Linux
+ Episodio [Compilando Podcast](https://mastodon.social/@podcastlinux/111061618451761311) sobre distros para programar
+ Acreditación al profesorado sobre [Competencia Digital](https://mastodon.social/@podcastlinux/111073886362465774)
+ [Creative Commons](https://mastodon.social/@podcastlinux/111089929427586938) en el curso de Competencia Digital
+ [Software Libre](https://mastodon.social/@podcastlinux/111101254701309414) de audio y vídeo en el curso de Competencia Digital
+ [Manic Miner](https://mastodon.social/@podcastlinux/111079548540893591) mi retrojuego fetiche
+ Si quieres aprender y mejorar en Ajedrez, [Lichess](https://mastodon.social/@podcastlinux/111113522497858829) es tu solución libre y gratuita
+ Busco usuarios/as de [Gentoo, Mandriva y Void Linux](https://mastodon.social/deck/@podcastlinux/111119184773512160)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
