---
title: "#161 Linux Express"
date: 2022-12-21
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/161linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 4910182
  mlength : 4872718
  iduration : "00:09:53"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE161
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/161linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/161linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #171 [ Godot, Motor de Videojuegos Libre](https://podcastlinux.com/PL171)
+ Siguiente episodio:Linux Connexion con [Rafa Laguna](https://twitter.com/RafaLagoon)
+ Próximo proyecto: [PodLi, the Podcast Linux Game](https://gitlab.com/podcastlinux/pocastlinuxgame)
+ Sigo probando con Arch Linux
+ ¿Fedora como distro de cabecera?
+ Dilema entre [X11 o Wayland](https://nitter.net/podcastlinux/status/1603273398452334592#m)
+ ¿AppImage para [Maldita Castilla](https://nitter.net/podcastlinux/status/1602536291933065218#m)?
+ [Kdenlive](https://kdenlive.org/en/2022/12/kdenlive-22-12-released/) versión 22.12
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
