---
title: "#174 Linux Express"
date: 2023-06-21
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/174linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6011567
  mlength : 5633916
  iduration : "00:11:35"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE174
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/174linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/174linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #184 [Especial Preguntas y Respuestas](https://podcastlinux.com/PL184)
+ Siguiente episodio: Especial 7º Aniversario
+ Teclado [MIDI Akai MIni MPK2](https://mastodon.social/@podcastlinux/110495387027324933)
+ Yoshimi, tu [sintetizador libre](https://mastodon.social/@podcastlinux/110506711718343900)
+ Probando [Tonality](https://mastodon.social/@podcastlinux/110518980093703100), un piano virtual con realce de escalas tonales
+ Probando [Hydrogen](https://mastodon.social/@podcastlinux/110535023245266235) con el controlador MIDI Akai Mini MPK2
+ Poder escuchar [Podcast Linux en Overcast](https://mastodon.social/@podcastlinux/110546347872785879)
+ Probando el [audio bluetooth de la tarjeta wifi china](https://mastodon.social/@podcastlinux/110558615989858886)
+ Buscando fuentes de [letras libres con licencias OFL](https://mastodon.social/@podcastlinux/110564278323309547)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
