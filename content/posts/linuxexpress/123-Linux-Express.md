---
title: "#123 Linux Express"
date: 2021-07-07
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/123linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5232851
  mlength : 5097054
  iduration : "00:10:36"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE123
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/123linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/123linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #132 [Especial 5º Aniversario](https://podcastlinux.com/PL132)
+ Siguiente episodio: Linux Connexion con David Marzal
+ Entrevistado en [Compilando Podcast](https://compilando.audio/index.php/2021/06/20/1411/)
+ [Slimbook One Ryzen](https://slimbook.es/one)
+ Actualizar KdeNeon desde la [terminal](https://masonbee.nz/updating-kde-neon/)
+ Requisitos mínimos de [Windows 11](https://hardzone.es/noticias/equipos/requisitos-pc-windows-11/)
+ Congreso [EsLibre 2021](https://eslib.re/2021/)
+ Actualización de [Telegram](https://telegram.org/blog/group-video-calls/es)
+ Podcast [Ubuntu y otras hierbas](https://www.ivoox.com/podcast-ubuntu-otras-hierbas_sq_f1412582_1.html)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
