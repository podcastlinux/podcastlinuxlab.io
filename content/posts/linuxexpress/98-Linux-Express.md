---
title: "#98 Linux Express"
date: 2020-07-22
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/98linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5658870
  mlength : 5533890
  iduration : "00:11:20"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE98
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/98linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/98linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #108 [Linux Connexion con Converso72](https://podcastlinux.com/PL108)
+ Siguiente episodio: Linux Connexion con [Karla Pérez](https://karlaperezyt.com/)
+ Actualizaciones en el feed: <https://podcastlinux.com/feed>
+ Feed en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6).
+ [Script](https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/-/blob/master/conversor.sh) para automatizar Podcast Linux.
+ Subiendo archivos a [Archive.org](https://archive.org/) desde la Terminal: <https://archive.org/services/docs/api/internetarchive/cli.html>
+ Nuevo juego libre de [Santi Ontañón](https://github.com/santiontanon/triton)
+ ¿Me quedo con el [Vant Edge](https://www.vantpc.es/edge)?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
