---
title: "#182 Linux Express"
date: 2023-10-11
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/182linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5544017
  mlength : 5236855
  iduration : "00:10:46"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE182
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/182linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/182linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #191 [Distros Inmutables](https://podcastlinux.com/PL192)
+ Episodio #192 Linux Connexion con [Eduardo Medina](https://www.muylinux.com/author/eduardo-medina)
+ Modificando pdfs con [PDF Mix Tool](https://mastodon.social/@podcastlinux/111129565727387295)
+ [Okular](https://mastodon.social/@podcastlinux/111158820962166668) para firmar pdfs
+ Rellenar pdfs digitalmente con [LibreOffice Draw](https://mastodon.social/deck/@podcastlinux/111198457376299032)
+ Mejora imágenes reescalándolas con IA con [Upscayl](https://mastodon.social/@podcastlinux/111140890780245561)
+ Tenemos nueva [Raspberry Pi 5](https://mastodon.social/@podcastlinux/111153158579861904)
+ Busco usuarios/as de [Gentoo y OpenMandriva](https://mastodon.social/@podcastlinux/111169201931922765)
+ [OpeningTree](https://mastodon.social/@podcastlinux/111180526468046267) para trabajar tu repertorio de aperturas
+ Ya somos 1.800 en [Mastodon](https://mastodon.social/deck/@podcastlinux/111192795164545170)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  
+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux/>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com/>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
