---
title: "#114 Linux Express"
date: 2021-03-03
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/114linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6793569
  mlength : 6457511
  iduration : "00:13:26"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE114
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/114linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/114linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #124 [Git](https://podcastlinux.com/PL124)
+ Siguiente episodio: Linux Connexion con [Almmudena García](https://twitter.com/almu_hs)
+ Experiencia de emisión en directo con [OBS Studio](https://obsproject.com/es/) en el colegio
+ Emisión en directo libre: Owncast, Peertube o Autisciti
+ Streaming de audio con [Sonobus](https://sonobus.net/)
+ Soy nuevo socio de [KDE España](https://www.kde-espana.org/involucrate)
+ Nuevo teclado [60% Red Thunder](https://es.aliexpress.com/item/1005001349505351.html)
+ Punto de acceso wifi [Comfast 1200](https://es.aliexpress.com/item/4000921067887.html)
+ Vuelve [Maratón Pod](http://maratonpod.com)
+ Evento online [Open Data Day A Coruña](https://odd2021acoruna.gitlab.io/)
+ Avanzando en la [FLISOL Tenerife 2021](https://flisol.info/FLISOL2021/Espana/Tenerife)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
