---
title: "#64 Linux Express"
date: 2019-04-03
author: Juan Febles
categories: [linuxexpress]
img: 2019/64linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/64linuxexpress
  olength : 4402703
  mlength : 5273368
  iduration : "00:10:59"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE64
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/64linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/64linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#74 Linux Express con JoséGDF](https://avpodcast.net/podcastlinux/josegdf)
+ Próximo episodio: Proyecto [PC Reciclado](https://twitter.com/search?f=tweets&vertical=default&q=%23PCReciclado&src=typd)
+ Proyecto: Montar [PC Reciclado](https://twitter.com/search?f=tweets&vertical=default&q=%23PCReciclado&src=typd) en el cole
+ Batalla de Bot Telegram entre [Atareao](https://www.atareao.es/) y [Ugeek](https://ugeek.github.io/)
+ Podcast [República Web](https://republicaweb.es/episodio/la-complicada-economia-del-codigo-abierto/)
+ Territorio f-Droid: [Riot.im](https://f-droid.org/en/packages/im.vector.alpha/)
+ Vuelve [Maratón Linuxero](https://maratonlinuxero.org/)
+ [FLISol 19](https://flisol.info/FLISOL2019) Tenerife


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-1026529/).

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
