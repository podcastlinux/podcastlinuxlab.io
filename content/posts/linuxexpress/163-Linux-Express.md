---
title: "#163 Linux Express"
date: 2023-01-18
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/163linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6052925
  mlength : 5756493
  iduration : "00:11:43"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
  - /LE163
---

<audio controls>
  <source src="https://archive.org/download/linuxexpress/163linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/163linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #173 [Especial PC Reciclado de la Audiencia](https://podcastlinux.com/PL173)
+ Siguiente episodio:Distros no Debianitas
+ Primer boceto de [PodLi](https://nitter.net/podcastlinux/status/1610146563472478208#m): The Podcast Linux Game
+ Quemando ISO con [dd](https://nitter.net/podcastlinux/status/1610871338159194117#m)
+ Regalos de [Reyes](https://nitter.net/podcastlinux/status/1611648837319368705#m)
+ Sincronizado Joplin con Nextcloud a través de la cuenta de [Disroot](https://nitter.net/podcastlinux/status/1612018773824868352#m)
+ Personalizando el [grub con un tema ArchLinux](https://nitter.net/podcastlinux/status/1612683152308199426#m)
+ Personalizando [Neofecth](https://nitter.net/podcastlinux/status/1613407928060444672#m)
+ Cambiando Rar por [7zip](https://nitter.net/podcastlinux/status/1614164034730827776#m) para comprimir
+ Evento [EsLibre](https://eslib.re/2023/) en Zaragoza el 5 y 6 de mayo


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
