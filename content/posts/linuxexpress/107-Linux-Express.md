---
title: "#107 Linux Express"
date: 2020-11-25
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/107linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5272233
  mlength : 5047317
  iduration : "00:10:30"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE107
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/107linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/107linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #117 [Personalizando GNU/Linux](https://podcastlinux.com/PL117)
+ Siguiente episodio: Linux Connexion con [24H24L](https://www.24h24l.org/
+ Aportación en la [Akademy-es](https://www.kde-espana.org/akademy-es-2020/programa)
+ Más funciones con [ffmpeg](https://twitter.com/podcastlinux/status/1329300992828071939)
+ Sobre la nueva arquitectura M1 de Apple.
+ Se acerca el Black Friday.
+ ¿Y si hacemos un Flisol 2021?

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
