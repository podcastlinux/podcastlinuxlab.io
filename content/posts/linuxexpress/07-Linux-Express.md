---
title: "#07 Linux Express"
date: 2017-02-22
author: Juan Febles
categories: [linuxexpress]
img: 2017/linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/07linuxexpress
  olength : 3498405
  mlength : 3249631
  iduration : "00:06:46"
tags: [audio, telegram, Linux Express,]
comments: true
aliases:
  - /LE07
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/07linuxexpress.mp3" type="audio/mpeg">
</audio>

El 7º Linux Express, los audios que comparto cada 2 semanas en [Telegram](https://t.me/podcastlinux) para ir alternando
los podcasts quincenales con éstos.
Aquí encontrarás información de lo que estoy proyectando y realizando mientras esperas a un nuevo podcast.
