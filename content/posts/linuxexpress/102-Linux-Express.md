---
title: "#102 Linux Express"
date: 2020-09-16
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/102linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 6069292
  mlength : 5932345
  iduration : "00:12:21"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE102
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/102linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/102linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #112 [Hardware Nuevo set de Escritorio](https://podcastlinux.com/PL112)
+ Siguiente episodio: Solucionando problemas en GNU/Linux
+ Nuevos ultraportátiles [Slimbook Esential](https://slimbook.es/essential)
+ Nuevos cachibaches chinos: [Capturadora HDMI](https://es.aliexpress.com/item/4000917130635.html) y [Mando inalámbrico Retro](https://es.aliexpress.com/item/32949582044.html)
+ Somos 6.000 en [Twitter](https://twitter.com/podcastlinux)
+ [Radar Covid](https://github.com/RadarCOVID) es Software Libre
+ Podcast de un oyente: [Todo me pasa a mí](https://www.ivoox.com/podcast-todo-me-pasa-a-mi_sq_f11024589_1.html)
+ Eventos:  [esLibre](https://eslib.re/2020/)  y [FLISOL A Coruña](https://flisol2020acoruna.gitlab.io/) en septiembre
+ Colaboración charla [HacktoberDay](https://hacktoberday.librelabucm.org/)
+ Evento [24H24L](https://www.24h24l.org/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
