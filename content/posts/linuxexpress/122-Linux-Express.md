---
title: "#122 Linux Express"
date: 2021-06-23
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://op3.dev/e,pg=d55ecf24-06ca-5448-90e3-0f5a4e666a65/archive.org/download/linuxexpress/122linuxexpress
  image: https://podcastlinux.gitlab.io/images/linuxexpress.png
  olength : 5856140
  mlength : 5476979
  iduration : "00:11:24"
tags: [audio, telegram, Linux Express]
comments: true
aliases:
    - /LE122
---
<audio controls>
  <source src="https://archive.org/download/linuxexpress/122linuxexpress.ogg" type="audio/ogg">
  <source src="https://archive.org/download/linuxexpress/122linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #132 [Especial Especial Es_Libre](https://podcastlinux.com/PL132)
+ Siguiente episodio: Especial 5º Aniversario
+ Ya tengo un [Thinkpad X260](https://twitter.com/podcastlinux/status/1405747198143303695)
+ [Jam](https://jamshelf.com/), alternativa libre a ClubHouse.
+ [Plasma 5.22](https://kde.org/announcements/plasma/5/5.22.1/)
+ Windows 11: ¿Oportunidad para GNU/Linux?
+ Podcast [Baúl Digital](https://anchor.fm/s/5efabdd4/podcast/rss)
+ Eventos a tener en cuenta: [Es_Libre](https://eslib.re/2021/) y [Akademy](https://akademy.kde.org/2021)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
