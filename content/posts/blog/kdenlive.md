---
title: "Curso Edición de vídeo con Kdenlive"
date: 2021-08-30
author: Juan Febles
categories: [curso, kdenlive]
img: 2021/kdenlive.png
tags: [curso, kdenlive, vídeo]
comments: true
aliases:
    - /kdenlive
---
![]({{< ABSresource url="/images/2021/kdenlive1.png" >}})  

Por fin me he animado a realizar un curso sobre [Kdenlive](https://kdenlive.org/es/).  

Los motivos de elegir Kdenlive son varios:
+ Es un proyecto de la [Comunidad KDE](https://kde.org/es/), del cual soy socio en [KDE España](https://www.kde-espana.org/).
+ Es Software Libre. Repeta las 4 libertades y podemos colaborar en este proyecto de muchas formas.
+ Multiplataforma: puedes instalarlo en GNU/Linux y Windows de manera muy sencilla.
+ Es una oportunidad para que los usuarios/as de plataformas privativas conozcan las posibilidades del Software Libre.
+ Suele haber cierta demanda para utilizar software de edición de vídeos: vídeotutoriales, producciones familiares, trabajos personales o de proyectos en formato vídeo...
+ Es un editor potente, aunque asuste al inicio. Nos servirá para casi la totalidad de proyectos de vídeos que ideemos.
+ Es gratuito.

---


**TEMAS DEL CURSO:**

+ **Tema 0:** Presentación del curso  
<iframe title="Curso kdenlive: Tema 0: Presentación del curso" src="https://fediverse.tv/videos/embed/6ef7aed4-e391-4d14-b713-0f90f3e812c2" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 1:** Descarga, instalación y documentación  
<iframe title="Curso Kdenlive: Tema 1: Descarga, instalación y documentación" src="https://fediverse.tv/videos/embed/05035ebc-e631-4de1-b13d-fb20d66aae56" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 2:** Organización, áreas y preferencias  
<iframe title="Curso Kdenlive: Tema 2: Organización, áreas y preferencias" src="https://fediverse.tv/videos/embed/bf5870a6-575f-4dad-a541-26df3d161fee" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 3:** Proyectos y clips
<iframe title="Curso Kdenlive: Tema 3: Proyectos y clips" src="https://fediverse.tv/videos/embed/68ce0847-b401-4fee-bf3a-06e5d5bd8a7d" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 4:** Línea de tiempo
<iframe title="Curso Kdenlive: Tema 4: Línea de tiempo" src="https://fediverse.tv/videos/embed/f76e11a0-d570-4476-853b-a2c055d19879" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 5:** Exportar vídeo
<iframe title="Curso Kdenlive: Tema 5: Exportar vídeo" src="https://fediverse.tv/videos/embed/4f992279-ddb1-4c85-835b-a189cde7f464" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 6:** Atajos de teclado
<iframe title="Curso Kdenlive: Tema 6: Atajos de teclado" src="https://fediverse.tv/videos/embed/78761d2c-12ef-4a0c-a89d-433e11fa8484" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 7:** Transiciones
<iframe title="Curso Kdenlive: Tema 7: Transiciones" src="https://fediverse.tv/videos/embed/6cf5c921-41cf-4f86-9a4d-bfa47c92442b" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 8:** Efectos de vídeo
<iframe title="Curso Kdenlive: Tema 8: Efectos de vídeo" src="https://fediverse.tv/videos/embed/d71b25a4-d723-4055-ba1b-2cd78e635c5b" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 9:** Efectos de audio
<iframe title="Curso Kdenlive: Tema 9: Efectos de audio" src="https://fediverse.tv/videos/embed/542ed94f-884b-46d5-9118-7e7dc21e09c9" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 10:** Corrección de color
<iframe title="Curso Kdenlive: Tema 10: Corrección de color" src="https://fediverse.tv/videos/embed/cdd749b2-60a9-4a9c-bd73-c893e87429cb" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 11:** Fotogramas clave
<iframe title="Curso Kdenlive: Tema 11: Fotogramas clave" src="https://fediverse.tv/videos/embed/2755cc6b-3fac-4348-aa87-266ae8e909a5" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 12:** Añadir títulos
<iframe title="Curso Kdenlive: Tema 12: Añadir títulos" src="https://fediverse.tv/videos/embed/f790bf16-1b7d-4992-9bcb-ef445adbc3c9" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 13:** Extra: Crear intro 
<iframe title="Curso Kdenlive: Tema 13: Extra Crear intro" src="https://fediverse.tv/videos/embed/bc451cf8-470b-4630-acb3-8a58c8a2ffeb" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 14:** Extra: Vídeo de fotos
<iframe title="Curso Kdenlive: Tema 14: Extra Vídeo de fotos" src="https://fediverse.tv/videos/embed/18ad4ff3-b8b6-473d-bcde-a0d7d68c5a92" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 15:** Extra: Voz en off
<iframe title="Curso Kdenlive: Tema 15: Voz en off" src="https://fediverse.tv/videos/embed/d93986f5-729e-47a3-bae4-51733bc6177a" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 16:** Extra: Editar voz con Audacity
<iframe title="Curso Kdenlive: Tema 16: Editar voz con Audacity" src="https://fediverse.tv/videos/embed/8aa5c6f3-7469-4cc7-ae85-cb3d98d96bd7" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 17:** Extra: Efecto Chroma
<iframe title="Curso Kdenlive: Tema 17: Efecto chroma" src="https://fediverse.tv/videos/embed/a081639f-af84-4b88-bd7d-6d31f88297dd" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 18:** Extra: Sincronizar sonido
<iframe title="Curso Kdenlive: Tema 18: Sincronizar audios" src="https://fediverse.tv/videos/embed/e03debc3-cab7-4140-9748-59a55ea667a3" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 19:** Extra: Vista multipista
<iframe title="Curso Kdenlive: Tema 19: Vista multipista" src="https://fediverse.tv/videos/embed/85022ab7-0dda-43d3-8c7c-33efda6a225a" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

+ **Tema 20:** Extra: Pixelar caras y objetos en movimiento
<iframe title="Curso Kdenlive: Tema 20: Pixelar caras y objetos en movimiento" src="https://fediverse.tv/videos/embed/0084c454-534e-4e87-aceb-d233f9551076" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

---


La imagen y vídeo de fondo utilizado están bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forman parte de [Pixabay](https://pixabay.com/images/id-1013873), [Pixabay](https://pixabay.com/videos/id-17771)  y [Pixabay](https://pixabay.com/videos/id-17246).


Toda la **música utilizada en este curso** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Podcast Linux - Intro Sound Theme: <https://archive.org/details/podcast-linux-intro-sound-theme>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />

