---
title: "Curso Edición de vídeo con Shotcut"
date: 2018-05-05
author: Juan Febles
categories: [curso, shotcut]
img: 2018/shotcut.png
podcast:
  audio: 
  video:
tags: [curso, shotcut, vídeo]
comments: true
aliases:
    - /shotcut
---
![]({{< ABSresource url="/images/2018/shotcut.png" >}})  
Después de hacer el curso de [Podcasting](https://podcastlinux.com/cursopodcasting/), me animo a realizar uno de edición de vídeo con [Shotcut](https://www.shotcut.org/).  

Los motivos de elegir Shotcut son varios:
+ Es Software Libre. Hay una gran comunidad detrás y nosotros mismos podemos colaborar en este proyecto.
+ Multiplataforma: puedes instalarlo en GNU/Linux, MacOS y Windows de manera muy sencilla.
+ Puede ser una buena excusa para que los usuarios de plataformas privativas conozcan las posibilidades del Software Libre.
+ Suele haber cierta demanda para utilizar software de edición de vídeos: vídeotutoriales, producciones familiares, trabajos personales en formato vídeo...
+ Es un editor sencillo. Nos servirá para casi la totalidad de proyectos de vídeos que ideemos.
+ Es gratuito.

**Tema 0**. Presentación del curso:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/ujdeOkz4TFY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


**Tema 1**. Elementos:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/wN2BvIemmHg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


**Tema 2**. Línea de Tiempo:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/rgkiFHGYlg8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Tema 3**. Exportar vídeo:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tuf5D2UNb3w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Tema 4**. Transiciones:  

<iframe width="560" height="315" src="https://www.youtube.com/embed/LizYfr61bpM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

La imagen y vídeo de fondo utilizado están bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forman parte de [Pixabay](https://pixabay.com/photo-1816353/) y [Pixabay](https://pixabay.com/videos/id-13232/).

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

