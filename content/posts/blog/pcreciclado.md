---
title: "Proyecto PC Reciclado de la Audiencia"
date: 2022-09-07
author: Juan Febles
categories: [reciclado, proyecto]
img: 2022/pcreciclado.png
tags: [proyecto, pc, reciclado]
comments: true
aliases:
    - /pcreciclado
---
![]({{< ABSresource url="/images/2022/pcreciclado.png" >}})  


Dona componentes de un PC para, entre todos, armar un ordenador de sobremesa multimedia GNU/Linux.

Objetivo: Demostrar que GNU/Linux funciona en máquinas en los que otros sistemas operativos las dan por obsoletas.


**Componentes:**
- <s>Caja o torre</s>
- <s>Fuente de alimentación: 500 w</s>
- <s>Placa base: ASUS M5A78L-M</s>
- <s>CPU o procesador: AMD FX-6300</s>
- <s>Memoria RAM: 4 módulos x 4GB = 16 GB RAM (Compatible con la placa base)</s>
- <s>Almacenamiento: SSD + HD</s>
- <s>Tarjeta gráfica: Nvidia 750 Ti 2GB DDR5</s>
- <s>2 monitores: DELL E1709W 17" 1440 x 900</s>
- <s>Teclado y ratón</s>
- <s>Altavoces</s>

No se pide dinero, sólo los componentes, si los tienes y quieres donarlos.  
No hay que comprar nada, sólo si lo tienes y quieres apoyar.

Es el proyecto más complicado que he realizado porque depende mucho de la voluntad de la audiencia. No sé si saldrá.  
La idea es que sirva como ordenador multimedia en el cole y demostrar que con GNU/Linux y componentes de hace 10 años se puede seguir disfrutando y utilizando un ordenador con Software Libre multimedia actual.

Si participas y quieres, se te nombrará en el episodio al terminar el proyecto.  

  
  
  
Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://fediverse.tv/a/podcastlinux/videos>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />

