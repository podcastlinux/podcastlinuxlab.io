---
title: "#179 Linux Connexion con Pedro Mosquetero Web"
date: 2023-04-05
author: Juan Febles
categories: [podcastlinux]
img: PL/PL179.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL179
  image: https://podcastlinux.gitlab.io/images/PL/PL179.png
  olength :
  mlength :
  iduration :
tags: [Linux Connexion, Pedro Mosquetero Web, Xeon, Kit Xeon]
comments: true
aliases:
  - /PL179
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL179.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL179.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega, la número 179 de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.

Hoy vuelve Pedro Mosquetero Web, profesor en Formación profesional en la rama de Computación y amante de GNU/Linux. Ha montado algunos Xeon y conoce bien las variedad de procesadores y kits y cómo se comportan con GNU/Linux.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast se aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.  


**Enlaces:**  

+ Web Mosquetero Web: <https://diario.mosqueteroweb.eu>
+ Twitter: <https://twitter.com/mosqueteroweb>
+ Análisis CPU: <https://www.cpubenchmark.net>

**La imagen utilizada en portada**, el logotipo de Pedro Mosquetero Web ha sido cedido expresamente para la carátula del episodio y es propiedad de él.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ Jay Someday - Happiness: <https://www.free-stock-music.com/jay-someday-happiness.html>


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
