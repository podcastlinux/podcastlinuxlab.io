---
title: "#147 Proyectos Podcast Linux"
date: 2022-01-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL147.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL147
  image: https://podcastlinux.gitlab.io/images/PL/PL147.png
  olength : 21305393
  mlength : 21837292
  iduration : "00:35:44"
tags: [PodcastLinux, Proyectos]
comments: true
aliases:
  - /PL147
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL147.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL147.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega de Podcast Linux, la número 147. Un saludo muy fuerte de quien te habla, Juan Febles.
Hoy vamos a echrale un vistazo a los proyectos que hemos realizado gracias a tu colaboración.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 147, Proyectos Podcast Linux.  

**Enlaces:**  
+ Web Podcast Linux: <https://www.podcastlinux.com>
+ Repositorio Web: <https://gitlab.com/podcastlinux/podcastlinux.gitlab.io>  
+ Maratón Linuxero: <https://gitlab.com/maratonlinuxero/maratonlinuxero.gitlab.io>  
+ FLISol Tenerife: <https://gitlab.com/osl-ull/flisol-tenerife>
+ Frente al Micrófono: <https://gitlab.com/podcastlinux/frentealmicrofono.gitlab.io>
+ Camiseta Podcast Linux: <https://gitlab.com/podcastlinux/camiseta>
+ Taller de Podcasting Libre: <https://gitlab.com/podcastlinux/tallerpodcasting>
+ Curso Kdenlive: <https://podcastlinux.com/kdenlive>
+ Charla Archive.org: <https://archive.org/details/charlaarchiveorg>
+ Charla ffmpeg: <https://commons.wikimedia.org/wiki/File:EsLibre_2021_P45_-_Juan_Febles_-_FFmpeg,_el_comando_%C3%BAnico_multimedia_para_controlarlo_todo.webm>
+ Viernes de Escritorio: <https://mastodon.social/tags/viernesdeescritorio> y <https://twitter.com/hashtag/ViernesDeEscritorio>

**La imagen utilizada en la portada**, se han liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/images/id-6367930/>.  

  
Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
PenguinMusic - Modern Chillout: <https://pixabay.com/es/music/optimista-penguinmusic-modern-chillout-12641/>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
