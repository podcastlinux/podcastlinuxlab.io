---
title: "#115 Hardware Slimbook Essential"
date: 2020-10-21
author: Juan Febles
categories: [podcastlinux]
img: PL/PL115.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL115
  image: https://podcastlinux.gitlab.io/images/PL/PL115.png
  olength : 16906470
  mlength : 17369414
  iduration : "00:27:58"
tags: [Hardware, Slimbook, podcastlinux]
comments: true
aliases:
    - /PL115
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL115.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL115.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 115. Un saludo muy fuerte de quien te habla, Juan Febles.  
Hoy tenemos un nuevo episodio Hardware, para dar a conocer un dispositivo GNU/Linux. Hace semanas que tengo el nuevo [Slimbook Essential](https://slimbook.es/essential), un ultraportátil con un precio asequible sin renunciar a la calidad que nos tiene acostumbrada la marca.

**Enlaces de interés:**  
Slimbook Essential: <https://slimbook.es/essential>  

**Imágenes:**  
![]({{< ABSresource url="/images/PL/SE1.png" >}}) 

![]({{< ABSresource url="/images/PL/SE2.png" >}})

![]({{< ABSresource url="/images/PL/SE3.png" >}}) 

![]({{< ABSresource url="/images/PL/SE4.png" >}})

![]({{< ABSresource url="/images/PL/SE5.png" >}}) 

![]({{< ABSresource url="/images/PL/SE6.png" >}})

![]({{< ABSresource url="/images/PL/SE7.png" >}})

**Vídeos:**  
Unboxing: <https://youtu.be/m7MQ1X6BMoo>  
Review inicial: <https://youtu.be/4bYfnG4qHiw>  

La **imagen** del Slimbook Essential es propiedad de su propia empresa y ha sido liberada con licencia [Creative Commons Atribución - Compartir igual ](https://creativecommons.org/licenses/by-sa/3.0/es/).  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
PMusicbyAden - Earth: <https://www.free-stock-music.com/musicbyaden-earth.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f120_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
