---
title: "Podcast Linux"
description: "El Podcast para amantes del Software Libre y GNU/Linux"
podcast_image: "logo_podcast.png"
podcast_guid: "bd8f7280-b35a-5bfd-bbde-829cf1b12682"
 # https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md#guid
---
El Podcast para amantes del Software Libre y GNU/Linux