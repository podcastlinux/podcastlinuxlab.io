---
title: "#178 Hardware Kit Xeon Chino"
date: 2023-03-22
author: Juan Febles
categories: [podcastlinux]
img: PL/PL178.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL178
  image: https://podcastlinux.gitlab.io/images/PL/PL178.png
  olength : 21766625
  mlength : 22862467
  iduration : "00:37:05"
tags: [Xeon, Placas Chinas, Kit Xeon]
comments: true
aliases:
  - /PL178
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL178.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL178.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega, la número 178, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.
Hoy voy a contarte mi experiencia con Kits Xeon Chinos y cómo se comportan con GNU/Linux.

Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 178 Kit Xeon Chino.

**Enlaces:**
+ CacharreoGeek: <https://cacharreogeek.es>
+ Post Pedro MosqueteroWeb: <https://diario.mosqueteroweb.eu/2020/08/ordenador-potente-por-150.html>
+ PlacasChinas.com: <https://placaschinas.com>
+ Calculadora Fuente de Alimentación: <https://www.geeknetic.es/calculadora-fuente-alimentacion>


**La imagen utilizada en la portada**, es una foto propia y tiene licencia [Creative Commons Atribución - Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/deed.es)

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ LiQWYD & Luke Bergs - Swing: <https://www.free-stock-music.com/liqwyd-luke-bergs-swing.html>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
