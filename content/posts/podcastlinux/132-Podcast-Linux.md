---
title: "#132 Especial Es_Libre 2021"
date: 2021-06-16
author: Juan Febles
categories: [podcastlinux]
img: PL/PL132.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL132
  image: https://podcastlinux.gitlab.io/images/PL/PL132.png
  olength : 40390021
  mlength : 41643784
  iduration : "01:09:09"
tags: [Especial, Es_Libre, podcastlinux]
comments: true
aliases:
  - /PL132
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL132.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL132.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega de Podcast Linux, la número 132. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy tenemmos un especial sobre el evento [Es_Libre](https://eslib.re/2021),  edición online organizada por [LibreLabUCM](https://librelabucm.org/) el 25 y 26 de junio.  
Tenemos con nosotros a tres de sus organizadores.  
- Paula de la Hoz (Co-fundadora y presidenta de Interferencias, en la organización de esLibre desde la primera edición)
- Germán Martínez (Co-fundador y vicepresidente de Interferencias, también en la organización de esLibre desde la primera edición)
- Rafael Mateus (Vicepresidente de Libre Lab UCM, asociación anfitriona de esLibre 2021)

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.



**Enlaces:**  
+ Web: <https://eslib.re/2021/>  
+ Correo: [hola@eslib.re](mailto:hola@eslib.re)  
+ Matrix: <https://matrix.to/#/#esLibre:matrix.org>  
+ Mastodon: <https://floss.social/@eslibre/>  
+ Twitter: <https://twitter.com/esLibre_>  
+ Grupo Telegram: <https://t.me/esLibre>  
+ LibreLab UCM: <https://librelabucm.org/>  
+ Interferencias: <https://interferencias.tech/>  

**La imagen utilizada en la portada**, el pingüino, es el logotipo de Es_Libre y es propiedad de este evento.
  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Roa Music - Sunrise: <https://www.free-stock-music.com/roa-music-sunrise.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
