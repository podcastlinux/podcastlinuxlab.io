---
title: "#187 Linux Connexion con Linuxero Errante"
date: 2023-07-26
author: Juan Febles
categories: [podcastlinux]
img: PL/PL187.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL187
  image: https://podcastlinux.gitlab.io/images/PL/PL187.png
  olength : 38741719
  mlength : 39590729
  iduration : "01:05:03"
tags: [Linux Connexion, Linuxero Errante, Juan Almiñana]
comments: true
aliases:
  - /PL187
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL187.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL187.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega, la número 187 de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.

Seguimos con las charlas de verano con Juan Almiñana, más conocido en la Linuxfera como Linuxero Errante. Se considera un lunático linuxero amante de la música y los tatuajes, le apasiona GNU Linux y el Software Libre. Comenzó en 2106 con un blog que cerró hace algún tiempo por falta de motivación y se pasó a Youtube. En su canal habla de GNU Linux y otros temas que le resulten interesantes, siempre como base el mundo linuxero . Es usuario doméstico de este sistema que tanto le gusta y la libertad y el buen rollo son dos bases importantes en este camino por el Software Libre. Es usuario de Plasma y de Arch y ahora está en EndeavourOS.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast se aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.  


**Enlaces:**  

+ Youtube: <https://www.youtube.com/c/JuanJJLinuxeroerrante>
+ Telegram: <https://t.me/linuxeroerrante54>
+ Grupo Telegram Linuxeroerrante: <https://t.me/+F8jiPQzlgL44NGY8>
+ Instagram: <https://www.instagram.com/jj.linuxeroerrante/?hl=es>
+ Audio-Rayadas: <https://www.ivoox.com/podcast-audio-rayadas-linuxeroerrante_sq_f11713427_1.html>

**La imagen utilizada en portada**, la imagen de Juan Almiñana ha sido cedida expresamente por él para la carátula del episodio y es propiedad suya.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ Surf House Productions - Island Breeze: <https://www.free-stock-music.com/surf-house-productions-island-breeze.html>


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
