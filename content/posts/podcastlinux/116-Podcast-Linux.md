---
title: "#116 Personalizando GNU/Linux"
date: 2020-11-04
author: Juan Febles
categories: [podcastlinux]
img: PL/PL116.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL116
  image: https://podcastlinux.gitlab.io/images/PL/PL116.png
  olength : 19831769
  mlength : 20115827
  iduration : "00:31:50"
tags: [podcastlinux]
comments: true
aliases:
  - /PL116
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL116.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL116.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otro episodio de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy complementamos los episodios de Pásate a GNU/Linux y solucionando problemas en GNU/Linux con Personalizando GNU/Linux.

**Enlaces de interés:**  
Partición de Datos: <https://salmorejogeek.com/2016/10/21/particion-de-datos-en-mediadatos-para-que-es-y-por-que-la-hago-en-linux%EF%BB%BF/>  
Crear llave SSH: <https://gitlab.uhu.es/cusl/tutoriales_libres/blob/master/git_ssh_tutorial/tutorial_gitssh.md>  
Repositorio web Podcast Linux: <https://gitlab.com/podcastlinux/podcastlinux.gitlab.io>  

**La imagen utilizada en la portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/photo-1816348/>  
La imagen del Ñu levitando, se ha liberado con licencia [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) y es propiedad de Nevrax Design Team : <https://www.gnu.org/graphics/meditate.html>  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Roa Music - Freedom: <https://www.free-stock-music.com/roa-music-freedom.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  
Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
