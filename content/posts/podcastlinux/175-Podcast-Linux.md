---
title: "#175 Linux Connexion Distros no Debianitas"
date: 2023-02-08
author: Juan Febles
categories: [podcastlinux]
img: PL/PL175.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL175
  image: https://podcastlinux.gitlab.io/images/PL/PL175.png
  olength : 33695299
  mlength : 35994192
  iduration : "00:59:20"
tags: [Linux Connexion, Distribuciones, Arch Linux, Fedora, OpenSUSE, Slackware]
comments: true
aliases:
  - /PL175
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL175.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL175.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega, la número 175 de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy vamos a hablar de Distribuciones no Debianitas con 4 linuxeros.

David Marzal, Administrador de sistemas GNU/Linux de profesión, apasionado por el software libre y la sostenibiliad de vocación. Muy activo en Asociaciones como GNU/Linux Valencia, KDE España o Residuo Cero de la Región de Murcia. Es usuario de Arch Linux.

Eduardo Medina es entusiasta del Software Libre de vanguardia, comparte tutoriales técnicos de software y es redactor en MuyLinux.com. Es usuario de Fedora.

David Vargas es Ingeniero Informático trabaja de profesor de FP Especialidad: Sistemas y Aplicaciones informáticas desde 2004. Es un apasionado de GNU/Linux y Ruby lover y usuario de OpenSUSE.

Javier Roggero es Linuxero desde 2004, es blogger, administra un blog de música celta y ferias Medievales, estudió producción musical, y también programa de manera autodidacta. Es usuario de Salix (una distro basada en Slackware).


Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast se aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.  

Únete al nuevo Proyecto PodLi (Podcast Linux, The Game): <https://gitlab.com/podcastlinux/pocastlinuxgame>. Anímate a participar.  


**Enlaces:**  

+ David Marzal: <https://mastodon.social/@DavidMarzalC>
+ Eduardo Medina: <https://mastodon.social/@edumedinalinux>
+ David Vargas: <https://ruby.social/@dvarrui>
+ Javier Roggero: <https://mast.lat/@jrr>

**La imagen utilizada en portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Pixabay](https://pixabay.com/es/service/terms/#license): <https://pixabay.com/images/id-1020231>
Los logotipos de las diferentes distribuciones GNU/Linux son propiedad de cada una de sus comunidades.


Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ Alex-Productions - The Last of Us: <https://www.free-stock-music.com/alex-productions-the-last-of-us.html>


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
