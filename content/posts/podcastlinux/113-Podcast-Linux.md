 ---
title: "#113 Solucionando problemas en GNU/Linux"
date: 2020-09-23
author: Juan Febles
categories: [podcastlinux]
img: PL/PL113.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL113
  image: https://podcastlinux.gitlab.io/images/PL/PL113.png
  olength : 19076994
  mlength : 20391891
  iduration : "00:33:29"
tags: [problemas, podcastlinux]
comments: true
aliases:
  - /PL113
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL113.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL113.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!! 
Bienvenido a otro episodio de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.  
Hoy vamos a seguir ayudando a migrar a GNU/Linux comentando los principales problemas a la hora de instalar este sistema operativo.  

Cualquier duda, aportación o aclaración la puedes hacer en [podcastlinux.com](https://podcastlinux.com), en el Grupo de Telegram [Pásate a GNU/Linux](https://t.me/pasategnulinux) o en las diferentes redes sociales.

* ¿Qué distro eligo para ordenadores antiguos con pocos recursos?: [Linux Mint](https://linuxmint.com/). [Linux Mint LMDE 32 bits](https://linuxmint.com/download_lmde.php), [MX Linux](https://mxlinux.org/) o [Bunsenlabs](https://www.bunsenlabs.org/).
* Crear un pendrive con la imagen de tu distro: [Etcher](https://www.balena.io/etcher/). Revisar el pendrive.
* Arrancar la imagen de tu distro: Boot Menu o Menú de Arranque (F8, F10, F11, F12)
* Dual boot: Copia de Seguridad. Instalar GNU/Linux junto a Windows Boot Manager (en principio no hacerlo manual)
* Reparar Grub: Problema al actualizar W.: <https://slimbook.es/tutoriales/linux/164-3-maneras-de-como-reinstalar-o-reparar-grub-boot-repair-archlinux-o-antergos>

~~~
sudo add-apt-repository ppa:yannubuntu/boot-repair 
sudo apt-get update 
sudo apt-get install -y boot-repair 
boot-repair  
~~~

* Controladores para la tarjeta wifi: Administrador de controladores > Activar controlador del wifi con el usb puesto. `lspci | grep Wireless`  
Atheros firmware-atheros  
Broadcom firmware-b43-installer, broadcom-sta-dkms, firmware-b43legacy-installer o firmware-brcm80211  
Intel Firmware-ipw2x00, firmware-intelwimax o firmware-iwlwifi  
Realtek firmware-realtek  
ti firmware-ti-connectivity  
ZyDas firmware-zd1211  

* Controladores tarjeta de vídeo: `lspci | grep VGA` Drivers privativos Nvidia o AMD. Administrador de controladores > Activar controlador
* Controladores para el touchpad: <https://www.maketecheasier.com/fix-touchpad-not-working-linux/> Ver si está desabilitado Fn+F7. Activar en Configuración Touchpad.
* No detecta la batería de laptop: `cat /proc/acpi/battery/BAT0/info` <https://askubuntu.com/questions/7485/battery-not-detected>
* No se oyen los altavoces/auriculares: `alsamixer` Alsa > Pulseaudio Está silenciada alguna salida. m = silenciar (mute)
* Permisos Root: Permisos de administrador. Abrir con permisos administrativos: "Privilegios elevados" en rojo
* Alternativas a programas privativos: <https://swiso.org> 
* Instalar programas: Centro de Software. `sudo apt install`
* Añadir repositorios:   
~~~
sudo apt-add-repository ppa:nombre-del-repositorio
sudo apt update
sudo apt install
~~~
* Añadir impresoras: Impresoras/Centro de Impresión. 
* Dispositivos USB: `lsusb` 
* Dispositivos bluetooth: <https://www.maketecheasier.com/setup-bluetooth-in-linux/>
* Kernel Panic: <https://askubuntu.com/questions/898449/kernel-panic-and-unable-to-boot-ubuntu-16-04-after-updating>  
~~~
mount -o remount,rw /
mkinitramfs -o /boot/initrd.img-{kerner_version}-generic {kernel_version}-generic
update-grub
~~~

Cualquier duda, aportación o aclaración la puedes hacer en [podcastlinux.com](https://podcastlinux.com), en el Grupo de Telegram [Pásate a GNU/Linux](https://t.me/pasategnulinux)  o en las diferentes redes sociales.


**La imagen utilizada en la portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-1020156/>  
 
Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Sarah Jansen Music - Miss You: <https://www.free-stock-music.com/sarah-jansen-music-miss-you.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Pásate a GNU/Linux: <https://t.me/pasategnulinux>
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
