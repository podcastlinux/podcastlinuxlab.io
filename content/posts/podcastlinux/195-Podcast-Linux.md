---
title: "#195 Linux Connexion Distros Madres II"
date: 2023-11-15
author: Juan Febles
categories: [podcastlinux]
img: PL/PL195.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL195
  image: https://podcastlinux.gitlab.io/images/PL/PL195.png
  olength : 42872138
  mlength : 44080623
  iduration : "01:12:46"
tags: [Linux Connexion, Distros, Distros Madres, Voro, Juan Almiñana]
comments: true
aliases:
  - /PL195
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL195.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL195.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega, la número 195 de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy vamos a hablar de Distros Madres con usuarios.

Voro MV , es miembro de GNU/Linux Valencia, bloguero, podcaster, Youtuber y un amante del motociclismo. Desde hace mucho tiempo utiliza Debian GNU/Linux.

Vicfred es Mejicano, trabaja en mountain view California en Google como ingeniero de software. Estudió matemáticas puras. Es el fundador de C++ México, streamea resolviendo problemas de programación en vivo. Ha usado gentoo como su distro principal desde hace 14 años.

Juan Almiñana, más conocido en la Linuxfera como Linuxero Errante, es un apasionado GNU Linux y el Software Libre. En su canal de Youtube habla de GNU/Linux y otros temas que le resulten interesantes, siempre como base el mundo linuxero . Es usuario doméstico de este sistema que tanto le gusta, y la libertad y el buen rollo son dos bases importantes en este camino por el Software Libre. Es usuario de Void Linux.

**Enlaces:**  
+ Vídeo instalación Gentoo: <https://www.youtube.com/live/B53jJqjwTl4>
+ Voro MV: <https://mastodon.online/@voromv>
+ Youtube Voro MV: <https://www.youtube.com/VOROMV>
+ Vicfred: <https://bsd.network/@Vicfred>
+ Twitch Vicfred: <https://www.twitch.tv/Vicfred>
+ Linuxero Errante: <https://twitter.com/JuanAlGr>
+ Youtube Linuxero Errante: <https://www.youtube.com/c/JuanJJLinuxeroerrante>

**La imagen utilizada en portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Pixabay](https://pixabay.com/es/service/terms/#license): <https://pixabay.com/images/id-1020231>.
Los logotipos de las diferentes distribuciones GNU/Linux son propiedad de cada una de sus comunidades.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ FSM Team & < e s c p > - Time Rider: <https://www.free-stock-music.com/time-rider.html>


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
