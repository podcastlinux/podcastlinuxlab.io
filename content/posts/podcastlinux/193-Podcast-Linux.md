---
title: "#193 Linux Connexion con Eduardo Medina"
date: 2023-10-18
author: Juan Febles
categories: [podcastlinux]
img: PL/PL193.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL193
  image: https://podcastlinux.gitlab.io/images/PL/PL193.png
  olength : 36426299
  mlength : 37884576
  iduration : "01:02:06"
tags: [Linux Connexion, Inmutables, Distros, Eduardo Medina]
comments: true
aliases:
  - /PL193
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL193.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL193.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 193. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy estamos con Eduardo Medina, entusiasta del Software Libre de vanguardia, comparte tutoriales técnicos de software y es redactor en MuyLinux.com. Es usuario de Fedora Silverblue y OpenSUSE MicroOS y nos trasladará toda su experiencia y conocimientos sobre las distribuciones inmutables.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y que este podcast se aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.  


**Enlaces:**  

+ Mastodon: <https://mastodon.social/@edumedinalinux>
+ Canal Youtube: <https://www.youtube.com/c/EduardoMedinaEdlinks>
+ Publicaciones en MuyLinux.com: <https://www.muylinux.com/author/eduardo-medina>


**La imagen utilizada en portada**, la imagen de Eduardo Medina, ha sido cedida expresamente por él para la carátula del episodio y es propiedad suya.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ Alex Productions - Minimal Corporate Starup: <https://www.free-stock-music.com/alex-productions-minimal-technology-corporate-startup.html>


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
