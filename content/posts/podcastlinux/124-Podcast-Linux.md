---
title: "#124 Git"
date: 2021-02-24
author: Juan Febles
categories: [podcastlinux]
img: PL/PL124.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL124
  image: https://podcastlinux.gitlab.io/images/PL/PL124.png
  olength : 17849090
  mlength : 18228711
  iduration : "00:30:06"
tags: [Linux Connexion, Almudena García, podcastlinux]
comments: true
aliases:
  - /PL124
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL124.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL124.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otro episodio de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy vamos a hablar de Git, un software de control de versiones diseñado, nada más y nada menos que por Linus Torvalds. Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 124, Git.  

**Comandos:**  
Git init  
git clone  
git config  
git remote  
git status  
git add  
git commit  
git push  
git pull  
git checkout  
git branch  
git merge  
git reset  
git revert  
git diff  

**Enlaces:**  
Taller Git: <https://gitlab.uhu.es/cusl/tutoriales_libres/blob/master/taller_git/taller_git.md>  
Buenas prácticas Git: <https://gitlab.uhu.es/cusl/tutoriales_libres/blob/master/tutorial_gitlab_proyectos/tutorial_gitlab_proyectos.md>  
Libro Git: <https://git-scm.com/book/es/v2>  
Repositorio web podcastlinux: <https://gitlab.com/podcastlinux/podcastlinux.gitlab.io>  
Gitlab Pages: <https://docs.gitlab.com/ee/user/project/pages/>  
Twitter Almudena García: <https://twitter.com/almu_hs>  

**Las imágenes utilizadas en la portada**, se han liberado desde [Pixabay](https://pixabay.com/) y [Git](https://git-scm.com) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-3289367/> y [Creative Commons Atribución](https://creativecommons.org/licenses/by/3.0/): <https://git-scm.com/images/logos/downloads/Git-Logo-White.eps> respectivamente.  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Electronic Senses - Never: <https://www.free-stock-music.com/electronic-senses-never.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
