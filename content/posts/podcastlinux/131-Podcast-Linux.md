---
title: "#131 Especial Directo con Software Libre"
date: 2021-06-02
author: Juan Febles
categories: [podcastlinux]
img: PL/PL131.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL131
  image: https://podcastlinux.gitlab.io/images/PL/PL131.png
  olength : 34620859
  mlength : 38307140
  iduration : "01:03:20"
tags: [Especial, Directo, podcastlinux]
comments: true
aliases:
  - /PL131
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL131.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL131.mp3" type="audio/mpeg">
</audio>  

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy tenemos un directo muy especial. En una hora hora estaré contigo para hablar de streaming, preguntas de los oyentes y alguna que otra anécdota.
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 131, Especial directo con Software Libre.

**Enlaces:**  
+ Vídeo Directo con Software Libre: <https://devtube.dev-wiki.de/videos/watch/cf16bb5b-c96e-4c7f-bf55-22452f4bd8c7>  
+ OBS Studio: <https://obsproject.com/es>  
+ Peertube: <https://joinpeertube.org/es>  
+ Peertube Podcast Linux: <https://devtube.dev-wiki.de/accounts/podcastlinux/video-channels>  
+ Kuttit: <https://kutt.it>  
+ Joplin: <https://joplinapp.org>  
+ Cryptpad: <https://cryptpad.fr>  
+ Viernes de escritorio: <https://twitter.com/hashtag/ViernesDeEscritorio>  
+ Mumble: <https://www.mumble.info/>  
+ Mornin: <https://mornin.fm/>  

**La imagen utilizada en la portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/es/illustrations/auriculares-m%C3%BAsica-trabajo-1816349/>  
Los logotipos OBS Studio y la mascota Peertube son marcas registradas de sus respectivas empresas.  
  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Peertube: <https://devtube.dev-wiki.de/accounts/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
