---
title: "#122 ESP8266"
date: 2021-01-27
author: Juan Febles
categories: [podcastlinux]
img: PL/PL122.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL122
  image: https://podcastlinux.gitlab.io/images/PL/PL122.png
  olength : 13455862
  mlength : 14792353
  iduration : "00:24:02"
tags: [Linux Connexion, Germán Martín, podcastlinux]
comments: true
aliases:
  - /PL122
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL122.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL122.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 122. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy tenemos un episodio sobre otra placa de desarrollo. En menos de media hora te comentaré qué es el ESP8266 y todas las posibilidades que tiene. Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 122, ESP8266.  

**Enlaces:**  
<https://programarfacil.com/>  
<https://programarfacil.com/podcast/esp8266-wifi-coste-arduino/>  
<https://programarfacil.com/podcast/nodemcu-tutorial-paso-a-paso/>  
<https://programarfacil.com/esp8266/como-programar-nodemcu-ide-arduino/>  
<https://programarfacil.com/blog/raspberry-pi/introduccion-node-red-raspberry-pi/>  
Twitter Germán Martín: <https://twitter.com/gmag12>  

**Las imágenes utilizadas en la portada**, se han liberado desde [Pixabay](https://pixabay.com/) y [Wikimedia](https://commons.wikimedia.org) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-5848806/> y [Creative Commons Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/): <https://commons.wikimedia.org/wiki/File:NodeMCU_Lua_Lol1n_V3.jpg> respectivamente.  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
DayFox & MusicbyAden - Poolside: <https://www.free-stock-music.com/musicbyaden-dayfox-poolside.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
