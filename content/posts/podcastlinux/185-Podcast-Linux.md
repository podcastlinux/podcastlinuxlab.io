---
title: "#185 Especial 7º Aniversario"
date: 2023-06-28
author: Juan Febles
categories: [podcastlinux]
img: PL/PL185.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL185
  image: https://podcastlinux.gitlab.io/images/PL/PL185.png
  olength : 20941676
  mlength : 21530707
  iduration : "00:35:23"
tags: [podcastlinux]
comments: true
aliases:
  - /PL185
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL185.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL185.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!
Bienvenido a otro episodio de Podcast Linux, el número 185. Un saludo muy fuerte de quien te habla, Juan Febles. Estamos de aniversario, Podcast Linux cumple 7 años.

Hoy quiero compartir contigo los cambios que se han realizado en esta temporada, revisando los mejores episodios y qué tengo preparado para el futuro, especialmente el episodio 200.

Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 185, Especial 7º Aniversario.


**La imagen utilizada en portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Pixabay](https://pixabay.com/es/service/terms/#license): <https://pixabay.com/photos/birthday-cake-cakes-cut-5176103>

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

+ Podcast Linux - Intro 22: <https://archive.org/details/intropodcastlinux22>
+ MusicbyAnden - Earth: <https://www.free-stock-music.com/musicbyaden-earth.html>

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Mastodon: <https://mastodon.social/@podcastlinux>
+ Correo: <podcastlinux@disroot.org>
+ Web: <https://podcastlinux.com>
+ Telegram: <https://t.me/podcastlinux>
+ Telegram Juan Febles: <https://t.me/juanfebles>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ FediverseTV: <https://fediverse.tv/a/podcastlinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
