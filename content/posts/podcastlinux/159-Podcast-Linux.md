---
title: "#159 Especial 6º Aniversario"
date: 2022-06-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL159.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL159
  image: https://podcastlinux.gitlab.io/images/PL/PL159.png
  olength : 16777907
  mlength : 17388965
  iduration : "00:28:10"
tags: [Especial, Aniversario, podcastlinux]
comments: true
aliases:
  - /PL159
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL159.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL159.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otro episodio de Podcast Linux, el número 159. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy estamos de aniversario, Podcast Linux cumple 6 años.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 159, Especial 6º Aniversario.  
Llevamos un año desde que el proyecto tomó un nuevo rumbo y voy a compartir contigo una valoración del año y futuros episodios para la 7ª temporada.  

**Enlaces de interés:**  
Gitlab: <https://gitlab.com/podcastlinux/podcastlinux.gitlab.io>  
Archive.org: <https://archive.org/details/@podcast_linux>  
HashOver: <https://www.barkdull.org/software/hashover>  
Joplin: <https://joplinapp.org/>  
Free Stock Music: <https://www.free-stock-music.com/>  
LMMS: <https://lmms.io/>  
Peertube Fediverse.tv: <https://fediverse.tv/a/podcastlinux>  

**La imagen utilizada en portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Pixabay](https://pixabay.com/es/service/terms/#license): <https://pixabay.com/images/id-1015655>  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Alex-Productions - Stylish Tech Corporate | Corporation: <https://www.free-stock-music.com/alex-productions-stylish-tech-corporate-corporation.html>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
FediverseTV: <https://fediverse.tv/a/podcastlinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
