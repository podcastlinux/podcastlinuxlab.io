---
title: "#112 Hardware Nuevo Set de Escritorio"
date: 2020-09-09
author: Juan Febles
categories: [podcastlinux]
img: PL/PL112.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL112
  image: https://podcastlinux.gitlab.io/images/PL/PL112.png
  olength : 15278599
  mlength : 16843085
  iduration : "00:27:05"
tags: [Hardware, podcastlinux]
comments: true
aliases:
  - /PL112
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL112.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL112.mp3" type="audio/mpeg">
</audio>  

¡¡¡Muy buenas amante del Software Libre!!! 
Bienvenido a otra entrega de Podcast Linux, la número 112. Un saludo muy fuerte de quien te habla, Juan Febles. 
Iniciamos la 5ª temporada con un episodio Hardware, las entregas sobre dispositivos GNU/Linux.

Nuevo ordenador de sobremesa:  
Caja: [AeroCoolOne Mini] (http://aerocool.es/producto/aero-one-mini-eclipse-minitorre-argb-copia/)  
Placa Madre: [MSI X470 Gaming Plus Max](https://www.msi.com/Motherboard/X470-GAMING-PLUS-MAX)  
Ventilación: 5 ventiladores [NOX HFAN 12 cm Led Blanco](https://www.amazon.es/NOX-Ventilador-HFAN-12cm-Espa%C3%B1a/dp/B01FSJXWL6?th=1)  
Fuente de alimentación: [UNIKA 600 80+ Modular](https://www.amazon.es/dp/B00OYTGH7S)  
Procesador: [AMD Ryzen 5 3600](https://www.amd.com/es/products/cpu/amd-ryzen-5-3600)  
Memoria: [16 Gb Hyper Fury Black DDR$ 2400MHZ](https://www.amazon.com/dp/B01D8U2B8W)  
Almacenamiento: [SSD M2 WD Blue 250 GB](https://www.amazon.com/dp/B073SBV3XX) + [HD Toshiba 2 TR 7200](https://www.amazon.es/dp/B009CPDI62)  
Gráfica: [AMD Gigabyte RX5500TX 8GB GDDR6](https://www.gigabyte.com/Graphics-Card/GV-R55XTGAMING-OC-8GD#kf)  

Otros periféricos:  
2 monitores [Philips 243V7QDSB](https://www.philips.co.uk/c-p/243V7QDSB_00/full-hd-lcd-monitor)  
Soporte [EWENT EW1512](https://www.appinformatica.com/ordenadores/tpv/tpvs-ewent-ew1512-soporte-escritorio-2-monitor-13-27-ticAPP_EW1512.html)  
Teclado + ratón inalámbrico [Logitech K270](https://www.logitech.com/es-es/product/wireless-keyboard-k270)  
Interfaz de Audio [Behringer U-Phoria UMC202HD](https://www.behringer.com/product.html?modelCode=P0BJZ)  
Micrófono dinámico [Behringer X8500](https://www.behringer.com/product.html?modelCode=P0120)  
Auriculares [SuperLux HD681B](https://es.aliexpress.com/w/wholesale-superlux%20hd681b.html)  


Las imágenes utilizadas en la portada, la caja, procesador y tarjeta gráfica son propiedad de AeroCool, AMD y Gigabyte respectivamente.  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Podcast Linux - 7 AM: <https://archive.org/details/PodcastLinux-7AM>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://t.me/podcastlinux) donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  
Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí. Respondo a todas ellas.

No te olvides suscribirte en [Ivoox](https://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2), [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9wb2RjYXN0bGludXguY29tL2ZlZWQ&ep=14) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast aloja su web en [Gitlab](https://gitlab.com/), un servicio libre de repositorios git y su contenido en [Archive.org](https://archive.org/), la biblioteca digital libre con licencias Creative Commons.

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
