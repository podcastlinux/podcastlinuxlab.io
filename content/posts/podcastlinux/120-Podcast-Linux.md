---
title: "#120 Hardware Vant Block Lite"
date: 2020-12-30
author: Juan Febles
categories: [podcastlinux]
img: PL/PL120.png
podcast:
  audio: https://op3.dev/e,pg=bd8f7280-b35a-5bfd-bbde-829cf1b12682/archive.org/download/podcast_linux/PL120
  image: https://podcastlinux.gitlab.io/images/PL/PL120.png
  olength : 14752722
  mlength : 16319849
  iduration : "00:26:44"
tags: [Hardware, Vant, podcastlinux]
comments: true
aliases:
    - /PL120
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL120.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL120.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 120. Un saludo muy fuerte de quien te habla, Juan Febles.  
Volvemos con cacharreo del bueno con un episodio Hardware para analizar un MiniPC GNU/Linux de bajo consumo: Vant Block Lite.  


**Enlaces de interés:**  
Vant Block Lite: <https://www.vantpc.es/block-lite>  
Vant Block: <https://www.vantpc.es/block>  
Teclado Mecánico RGB Linuxero: <https://www.vantpc.es/producto/teclado-mecanico-rgb-deeogaming-deepsolid-edicion-linuxera>  
Cargador 60W USB-C: <https://coolbox.es/accesorios-m%C3%B3viles-port%C3%A1tiles-y-tablets/3595-cargador-60w-con-powerdelivery-compatible-con-smartphones-tablets-y-ordenadores-port%C3%A1tiles.html>  
Powerbank 45W 20100mAh: <https://coolbox.es/cargadores-y-bater%C3%ADas/3582-powerbank-45w-20100mah-con-usb-c-powerdelivery-qc30-y-pantalla-lcd-8436556143403.html>  

**Vídeos:**  
MegaPack Productos Vant: <https://youtu.be/gh7D6vvuRc0>  
Unboxing: <https://youtu.be/vzsM0P4AAXo>  
Análisis inicial: <https://youtu.be/PrnmKFMm3-E>  

La **imagen** del Vant Block Lite es propiedad de su propia empresa y ha sido cedida para este episodio.  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
JayJen & Lichu - Tropical Surfing: <https://www.free-stock-music.com/jayjen-lichu-tropical-surfing.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f120_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://podcastlinux.gitlab.io/images/CC/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
