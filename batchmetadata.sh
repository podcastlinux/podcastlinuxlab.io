#!/bin/bash
set -eEuo pipefail

HL='\033[1;32m'   # Bold Green
WL='\033[1;33m'   # Bold Yellow
NL='\033[0m'      # Text Reset

GITLAB="$HOME/Documentos/podcastlinux.gitlab.io"
GITLAB="$HOME/gitlab/podcastlinux.gitlab.io"
ogg=${ogg:-}

metadatos() {
    RUTAART="$GITLAB/content/posts/$SECCION/${i}-$ARTICULO"

    if [[ $FORMATO == "ogg" ]]; then
        olength=$(wc -c "$DESTINO.ogg" | awk '{print $1}')
        sed -i "s/olength.*/olength : $olength/" "$RUTAART" || echo -e "${WL} No existe el fichero: $RUTAART ${NL}"
    else
        mlength=$(wc -c "$DESTINO.mp3" | awk '{print $1}')
        sed -i "s/mlength.*/mlength : $mlength/" "$RUTAART" || echo -e "${WL} No existe el fichero: $RUTAART ${NL}"
    fi
    iduration=$(ffprobe -hide_banner -i "$DESTINO.$FORMATO" 2>&1 | grep Duration| cut -c 13-20)
    sed -i "s/iduration.*/iduration : \"$iduration\"/" "$RUTAART" || echo -e "${WL} No existe el fichero: $RUTAART ${NL}"
}


echo -e "${HL}¿${WL}(P)${HL}odcast Linux o ${WL}(L)${HL}inux Express: ${NL}"
read -r podcast
echo -e "${HL}¿Hasta qué episodio a convertir?:${NL}"
read -r episodio
if [[ $podcast == "P" ]] ;then
    SECCION='podcastlinux'
    ARTICULO='Podcast-Linux.md'

    for i in $(seq -f "%02g" 00 "$episodio"); do 
        DESTINO="$HOME/gPodder/Downloads/Podcast Linux/PL${i}"
        for FORMATO in ogg ; do
            metadatos
        done
    done
elif [[ $podcast == "L" ]] ;then
    SECCION='linuxexpress'
    ARTICULO='Linux-Express.md'

    for i in $(seq -f "%02g" 01 "$episodio"); do 
        DESTINO="$GITLAB/static/Linux-Express/${i}linuxexpress"
        for FORMATO in mp3 ogg ; do
            metadatos
        done
    done
    


    for FORMATO in mp3 ogg ; do
        metadatos
    done
else
    echo -e "${WL}Debes elegir un Podcast${NL}"
fi

echo -e "${HL}FIN!${NL}"
